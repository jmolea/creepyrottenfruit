package com.crf.creepyrottenfruit.hud;

import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.text.Text;


/**
 * Created by Jose Maria Olea on 21/01/2018.
 */

public class FadingText implements IUpdateHandler {

    public Entity mText;
    private GameScene scene;

    public FadingText(Entity text, GameScene scene) {
        this.scene=scene;
        mText = text;
    }

    @Override
    public void onUpdate(float pSecondsElapsed)
    {
        if (scene.gameState == GameScene.GameState.PLAYING) {
            mText.setAlpha(mText.getAlpha() - .005f);
            mText.setY(mText.getY() + 10 * pSecondsElapsed);
            if (mText.getAlpha() <= 0) {
                mText.detachSelf();
                scene.unregisterUpdateHandler(this);
            }
        }
    }

    @Override
    public void reset() {
        // Reset code here
    }

}
