package com.crf.creepyrottenfruit.hud;

import org.andengine.entity.Entity;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;

/*
 * Created by Jose Maria Olea on 08/01/2018.
 */

public class ShadowText extends Entity {
    // ====================================================
    // CONSTANTS
    // ====================================================
    private final float SHADOW_ALPHA = .5f;
    private final int SHADOW_OFFSET = 5;
    // ====================================================
    // VARIABLES
    // ====================================================

    private Text mainText, shadowText;

    // ====================================================
    // CONSTRUCTOR
    // ====================================================

    public ShadowText (float x, float y, Font font, String text, TextOptions textOptions, VertexBufferObjectManager vbom) {
        super();

        mainText = new Text(x,y,font,text,textOptions,vbom);
        shadowText = new Text(x + SHADOW_OFFSET,y - SHADOW_OFFSET,font,text,textOptions,vbom);
        shadowText.setColor(Color.BLACK);
        shadowText.setAlpha(SHADOW_ALPHA);

        this.attachChild(shadowText);
        this.attachChild(mainText);

    }
    // ====================================================
    // METHODS
    // ====================================================

    public void setText(String newText){
        shadowText.setText(newText);
        mainText.setText(newText);
    }

    public void setColor (Color newColor) {
        mainText.setColor(newColor);
    }

    @Override
    public void setAlpha(float alpha) {
        super.setAlpha(alpha);
        this.mainText.setAlpha(alpha);
        this.shadowText.setAlpha(alpha);
    }
}
