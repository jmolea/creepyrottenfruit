package com.crf.creepyrottenfruit.hud;

import android.util.DisplayMetrics;
import android.util.Log;

import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.core.SpritePool;
import com.crf.creepyrottenfruit.entities.Onagre;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.util.GLState;
import org.andengine.util.adt.color.Color;
import org.andengine.util.math.MathUtils;

/*
 * Created by Jose Maria Olea on 24/11/2017.
 */

public class PowerSlider extends Entity implements ITouchArea {

    // ====================================================
    // CONSTANTS
    // ====================================================

    final float SIMULATION_CORRECTION_FACTOR = 0.56f; //MAGIC, do not touch
    final float HINT_DOTS_SPACING_FACTOR = 7;



    // ====================================================
    // VARIABLES
    // ====================================================

    public GameScene mScene;
    private SpritePool dotsPool;
    private float mValue, cropScreenCoordinatesY;
    private float verticalMargin;
    private Entity touchArea, button;
    private Entity parabolicPreview;
    private Entity sliderFrame, sliderBg;
    private boolean isButtonPressed;
    private float [] cameraToScreenRatio;
    private float lastPressedAreaY;
    private float dotX, dotY, dotDX, dotDY;


    public PowerSlider(float x, float y, float width, float height, GameScene scene) {
        super();

        mScene = scene;

        //button = new Sprite(x,y, ResourcesMgr.shotButtonTexture, ResourcesMgr.getInstance().vertexBufferObjectManager);
        mValue = .5f;

        touchArea = new Rectangle(x, y, width, height, ResourcesMgr.getInstance().vertexBufferObjectManager);
        touchArea.setVisible(false);

        //Get the display metrics object.
        DisplayMetrics displayMetrics  = new DisplayMetrics();
        ResourcesMgr.getInstance().activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        cameraToScreenRatio = new float [] { (float)displayMetrics.widthPixels / Constants.CAMERA_WIDTH, (float) displayMetrics.heightPixels / Constants.CAMERA_HEIGHT};


        if (Constants.DEBUG_MODE){
            touchArea.setVisible(true);
            touchArea.setColor(Color.RED);
            touchArea.setAlpha(.5f);
        }
        sliderBg = new Sprite (x,y, ResourcesMgr.powerSliderBgTexture, ResourcesMgr.getInstance().vertexBufferObjectManager) {
            @Override
            public void onManagedDraw(final GLState pGLState, final Camera pCamera) {

                pGLState.pushModelViewGLMatrix();
                {
                    pGLState.enableScissorTest();
                    pGLState.glPushScissor(0,0, 300, (int) (cropScreenCoordinatesY ));

                    super.onManagedDraw(pGLState, pCamera);

                    pGLState.glPopScissor();
                    pGLState.disableScissorTest();

                }
                pGLState.popModelViewGLMatrix();

            }
        };

        sliderFrame = new Sprite (x,y, ResourcesMgr.powerSliderFrameTexture, ResourcesMgr.getInstance().vertexBufferObjectManager);
        verticalMargin = (touchArea.getHeight() - sliderBg.getHeight())/2;

        this.attachChild(sliderBg);
        this.attachChild(sliderFrame);
        this.attachChild(touchArea);

        parabolicPreview = new Entity();
        scene.getChildByIndex(Constants.LAYER_EFFECTS).attachChild(parabolicPreview);

        // Sprite pool for all the dots used in the parabolic preview
        dotsPool = new SpritePool(ResourcesMgr.dotTexture,ResourcesMgr.getInstance().vertexBufferObjectManager);

    }

    public void drawParabolicPreview (float originX, float originY, float power, float angle) {
        //Log.d(Constants.TAG, "Draw parabollic: " + originX + ", " + originY);
        dotX = originX;
        dotY = originY;

        dotDX = (float) Math.cos(angle) * power * SIMULATION_CORRECTION_FACTOR;
        dotDY = (float) Math.sin(angle) * power * SIMULATION_CORRECTION_FACTOR;


        while (dotY>0) {
            //Log.d(Constants.TAG, "Draw parabollic dot: " + x + ", " + y);
            Sprite dot = dotsPool.obtainPoolItem(dotX,dotY);
            if (!dot.hasParent()) { // New dot created
                dot.setAlpha(.6f);
                parabolicPreview.attachChild(dot);
            }

            dotX += dotDX*HINT_DOTS_SPACING_FACTOR;
            dotY += dotDY*HINT_DOTS_SPACING_FACTOR;
            dotDY -= .1 * HINT_DOTS_SPACING_FACTOR;

        }
    }


    public void removeParabolicPreview (){
        // Remove dots
        for (int i=0;i<parabolicPreview.getChildCount();i++){
            Sprite dot = (Sprite)parabolicPreview.getChildByIndex(i);
            if (dot.isVisible()) {
                dotsPool.recyclePoolItem((Sprite) parabolicPreview.getChildByIndex(i));
            }
        }
    }

    @Override
    public float[] convertSceneCoordinatesToLocalCoordinates(float pX, float pY) {
        return touchArea.convertSceneCoordinatesToLocalCoordinates(pX, pY);
    }

    @Override
    public float[] convertLocalCoordinatesToSceneCoordinates(float pX, float pY) {
        return touchArea.convertLocalCoordinatesToSceneCoordinates(pX, pY);
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        //Log.d(Constants.TAG, "touchAreaLocalY = " + pTouchAreaLocalY);
        Onagre onagre = mScene.onagre;


        // If finger is down
        if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN) {
            if (!isButtonPressed) {
                isButtonPressed = true;
                /*ResourcesMgr.woodCreakSound.setRate(MathUtils.random(.9f,1.1f));
                ResourcesMgr.woodCreakSound.play();*/

            }
        }


        if (isButtonPressed && lastPressedAreaY != pTouchAreaLocalY) {
            if (Math.abs(pTouchAreaLocalY - lastPressedAreaY) > 3 ) { // Only calculate if the finger moves more than 3 pixels
                // Calculate slider value entered
                float bgTouchAreaLocalY = pTouchAreaLocalY - verticalMargin;
                mValue = bgTouchAreaLocalY / sliderBg.getHeight();
                mValue = Math.min(1, Math.max(0, mValue));


                // Update power slider visual
                //Log.d(Constants.TAG, "mValue = " + (mValue));
                cropScreenCoordinatesY = pTouchAreaLocalY * cameraToScreenRatio[1];


                // Show parabollic hint
                removeParabolicPreview ();
                float shotOriginX = onagre.body.mSprite.getX() + onagre.LAUNCH_POSITION_OFFSET_X;
                float shotOriginY = onagre.body.mSprite.getY() + onagre.LAUNCH_POSITION_OFFSET_Y;
                float shotPower = onagre.LAUNCH_MIN_POWER + mValue * onagre.LAUNCH_MIN_MAX_RANGE;
                drawParabolicPreview(shotOriginX, shotOriginY, shotPower, onagre.SHOOTING_ANGLE_RAD);

                // Store last value to avoid an unnecessary refresh of the parabollic
                lastPressedAreaY = pTouchAreaLocalY;
            }

        }


        // If finger is up, remove parabolic hint and shootWhenReady a projectile
        if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP && isButtonPressed) {
            Log.d(Constants.TAG, "Throw projectile: " + this.getValue());
            isButtonPressed = false;
            ResourcesMgr.woodCreakSound.stop();
            removeParabolicPreview ();

            onagre.shootWhenReady(mValue);
            Log.d(Constants.TAG, "Dots pool size="+dotsPool.getAvailableItemCount());
        }
        return true;
    }

    @Override
    public boolean contains(float pX, float pY) {
        return touchArea.contains(pX,pY);
    }

    public void setValue(float mValue) {
        this.mValue = mValue;
    }

    public float getValue() {
        return mValue;
    }

}
