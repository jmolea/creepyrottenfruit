package com.crf.creepyrottenfruit.hud;

import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.particle.SpriteParticleSystem;
import org.andengine.entity.particle.emitter.CircleParticleEmitter;
import org.andengine.entity.particle.initializer.ColorParticleInitializer;
import org.andengine.entity.particle.initializer.RotationParticleInitializer;
import org.andengine.entity.particle.initializer.ScaleParticleInitializer;
import org.andengine.entity.particle.initializer.VelocityParticleInitializer;
import org.andengine.entity.particle.modifier.AlphaParticleModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.util.Constants;
import org.andengine.util.math.MathUtils;

import java.util.ArrayList;
import java.util.Iterator;

/*
 * Created by Jose Maria Olea on 19/01/2018.
 */

public class ItemsBar extends Entity {

    // ====================================================
    // CONSTANTS
    // ====================================================
    private final static float ITEMSBAR_HEIGHT = 550;
    private final static float ITEMSBAR_WIDTH = 100;
    private final static float ITEM_SEPARATION = 20;
    private final static float ITEM_HEIGHT = 100;

    // ====================================================
    // VARIABLES
    // ====================================================
    private ArrayList<Item> items;
    private Item nextItem;
    public Item selectedItem;
    private int itemCountdown, enemiesKilled;
    private GameScene mScene;
    private SpriteParticleSystem particleSystem;

    // ====================================================
    // CONSTRUCTOR
    // ====================================================
    public ItemsBar (float x, float y, int killsPerItem, GameScene scene) {
        super (x, y,ITEMSBAR_WIDTH, ITEMSBAR_HEIGHT);
        this.itemCountdown = killsPerItem;
        mScene = scene;
        items = new ArrayList<Item>();

        addNextRandomItem ();
        repositionItems();

    }


    // ====================================================
    // METHODS
    // ====================================================
    public void selectItem (Item item) {

        ResourcesMgr.clickSound.play();

        if (selectedItem == item) {
            item.setScale(1f);
            selectedItem = null;
        }else {
            item.setScale(1.5f);
            selectedItem = item;
        }

    }

    public void removeSelectedItem () {
        items.remove(selectedItem);
        selectedItem.detachSelf();
        selectedItem = null;
        repositionItems();

    }

    public void winItem () {
        if (items.size() < mScene.MAX_ITEMS) {
            nextItem.setAlpha(1f);
            addItem(nextItem);
            addNextRandomItem();
            repositionItems();
        }
    }

    public void addItem (Item newItem) {

        items.add(newItem);

        //Splash

        if (particleSystem ==  null) {
            CircleParticleEmitter particleEmitter = new CircleParticleEmitter(newItem.getX(), newItem.getY(),50);
            particleSystem = new SpriteParticleSystem(particleEmitter, 1000, 1000, 20, ResourcesMgr.objectBitTexture, ResourcesMgr.getInstance().vertexBufferObjectManager);
            particleSystem.addParticleInitializer(new ScaleParticleInitializer<Sprite>(.3f, .8f));
            particleSystem.addParticleInitializer(new ColorParticleInitializer<Sprite>(newItem.color));
            particleSystem.addParticleInitializer(new VelocityParticleInitializer<Sprite>(-60,60,-50,50));
            particleSystem.addParticleInitializer(new RotationParticleInitializer<Sprite>(0,360f));
            particleSystem.addParticleModifier(new AlphaParticleModifier<Sprite>(0, 2f, 1f, 0f));
            this.attachChild(particleSystem);
        } else {
            ((CircleParticleEmitter)particleSystem.getParticleEmitter()).setCenter(newItem.getX(), newItem.getY());
            particleSystem.addParticleInitializer(new ColorParticleInitializer<Sprite>(newItem.color));
            particleSystem.reset();
        }
    }



    public void repositionItems () {

        int i = 0;
        Iterator it = items.iterator();

        while (it.hasNext()) {
            float verticalOffset = (i * (ITEM_HEIGHT + ITEM_SEPARATION));
            ((Item) it.next()).setY( - verticalOffset + ITEMSBAR_HEIGHT - ITEM_HEIGHT/2);
            i++;
        }

        float verticalOffset = (i * (ITEM_HEIGHT + ITEM_SEPARATION));
        nextItem.setY(-verticalOffset + ITEMSBAR_HEIGHT - ITEM_HEIGHT / 2);

        if (items.size() == mScene.MAX_ITEMS) {
            nextItem.setVisible(false);
        } else {
            nextItem.setVisible(true);
        }

    }

    public void addNextRandomItem () {

        Item.Type type = null;
        do {
            switch (MathUtils.random(9)) {
                case 0:
                case 1:
                case 2:
                case 3:
                    if (mScene.levelDef.itemDynamite) {
                        type = Item.Type.DYNAMITE;
                    }
                    break;
                case 4:
                case 5:
                case 6:
                    if (mScene.levelDef.itemCuttlery) {
                        type = Item.Type.CUTTLERY;
                    }
                    break;
                case 7:
                case 8:
                    if (mScene.levelDef.itemClock) {
                        type = Item.Type.CLOCK;
                    }
                    break;
            }
        } while (type == null);
        nextItem = new Item(ITEMSBAR_WIDTH / 2, 0, type, mScene);
        nextItem.setAlpha(.5f);
        this.attachChild(nextItem);

    }


    public Item.Type getSelectedType () {
        if (selectedItem != null) {
            return selectedItem.type;
        }
        return null;
    }

    public void notifyKill () {
        /* Add random item every N kills */
        enemiesKilled ++;
        itemCountdown --;
        if (itemCountdown <= 0 && countItems() < mScene.MAX_ITEMS) {
            ResourcesMgr.getPrizeSound.play();
            winItem();
            itemCountdown = mScene.levelDef.killsPerItem;
        }

    }

    public int countItems () {
        return items.size();
    }

    @Override
    public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
        if (pSceneTouchEvent.isActionDown()) {

            final float[] touchAreaSceneCoordinates = this.convertLocalCoordinatesToSceneCoordinates(pTouchAreaLocalX, pTouchAreaLocalY);
            final float touchAreaSceneX = touchAreaSceneCoordinates[Constants.VERTEX_INDEX_X];
            final float touchAreaSceneY = touchAreaSceneCoordinates[Constants.VERTEX_INDEX_Y];

            for (Item item : this.items) {
                if (item.contains(touchAreaSceneX, touchAreaSceneY)) {
                    selectItem(item);
                }
            }
        }
        return true;
    }
}
