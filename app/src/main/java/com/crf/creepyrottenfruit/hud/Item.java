package com.crf.creepyrottenfruit.hud;

import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.entity.sprite.TiledSprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.util.adt.color.Color;

/**
 * Created by Jose Maria Olea on 19/01/2018.
 */

public class Item extends TiledSprite {
    // ====================================================
    // ENUMS
    // ====================================================

    public enum Type {
        CLOCK,
        DYNAMITE,
        CUTTLERY
    }

    public Color color;

    // ====================================================
    // CONSTANTS
    // ====================================================

    // ====================================================
    // VARIABLES
    // ====================================================
    GameScene mScene;
    public Type type;

    // ====================================================
    // CONSTRUCTOR
    // ====================================================
    public Item (float x, float y, Type type, GameScene scene) {
        super(x, y, ResourcesMgr.itemsTexture, ResourcesMgr.getInstance().vertexBufferObjectManager);
        this.mScene = scene;
        this.type = type;

        switch (type) {
            case CLOCK:
                setCurrentTileIndex(0);
                color = new Color (159f/255,225f/255,202f/255);

                break;
            case DYNAMITE:
                setCurrentTileIndex(1);
                color = new Color (204f/255,127f/255,85f/255);
                break;

            case CUTTLERY:
                setCurrentTileIndex(2);
                color = new Color (213f/255,198f/255,114f/255);
                break;
        }


    }


    // ====================================================
    // METHODS
    // ====================================================


}
