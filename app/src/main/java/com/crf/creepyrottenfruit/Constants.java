package com.crf.creepyrottenfruit;

/*
 * Created by Jose Maria Olea on 12/11/2017.
 */

public class Constants {
    // General
    public static final String TAG = "CreepyRottenFruit";
    public static final String VERSION = "v.1.1";
    public static final boolean DEBUG_MODE = false;
    public static final boolean SHOW_FPS = false;
    public static final int START_AT_LEVEL = 0;
    public static final float PHYSICS_TIME_COMPRESSION = 1f;


    // Camera
    public static final int CAMERA_WIDTH = 1280;
    public static final int CAMERA_HEIGHT = 768;
    public static final float CAMERA_MAX_VELOCITY = 600f;


    // Layers

    public static final int LAYER_BACKGROUND = 0;
    public static final int LAYER_ITEMS = 1;
    public static final int LAYER_CHARACTERS = 2;
    public static final int LAYER_EFFECTS = 3;
    public static final int LAYER_DEBUG = 4;



    // Game dinamycs
    public static final float PROJECTILE_VELOCITY_TO_KILL = 6f;
}
