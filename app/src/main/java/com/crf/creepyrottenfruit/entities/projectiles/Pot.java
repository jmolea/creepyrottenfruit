package com.crf.creepyrottenfruit.entities.projectiles;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.entities.ObjectBit;
import com.crf.creepyrottenfruit.entities.enemies.Enemy;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.util.adt.color.Color;
import org.andengine.util.math.MathUtils;

import static org.andengine.extension.physics.box2d.util.constants.PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;

/*
 * Created by Jose Maria Olea on 10/11/2017.
 */

public class Pot extends Projectile{

    // ====================================================
    // CONSTANTS
    // ====================================================
    private static final float DENSITY = .8f;
    private static final float ELASTICITY = .02f;
    private static final float FRICTION = .65f;
    private final Color COLOR = new Color (133f/255,67f/255,39f/255);

    // ====================================================
    // CONSTRUCTOR
    // ====================================================
    public Pot(float x, float y, float powerX, float powerY, GameScene scene) {
        mPhysicsWorld = scene.mPhysicsWorld;
        mScene = scene;

        //final float height = 100;
        mEntity = new Sprite(x, y, ResourcesMgr.potTexture, ResourcesMgr.getInstance().vertexBufferObjectManager);
        mEntity.setCullingEnabled(true);
        //mEntity.setHeight(height);

        mPhysicsConnector = createPhysics(mPhysicsWorld, powerX, powerY);

        // Put the projectile in our world
        mScene.getChildByIndex(Constants.LAYER_ITEMS).attachChild(mEntity);
        mPhysicsWorld.registerPhysicsConnector(mPhysicsConnector);

        mEntity.registerUpdateHandler(this);


    }

    public PhysicsConnector createPhysics(PhysicsWorld physicsWorld, float powerX, float powerY) {

        final float angularVelocity = (float) (Math.PI * (Math.random() - .5d));

        float width = mEntity.getScaleX() * mEntity.getWidth()/PIXEL_TO_METER_RATIO_DEFAULT;
        float height = mEntity.getScaleY() *mEntity.getHeight()/PIXEL_TO_METER_RATIO_DEFAULT;

        Vector2[] vertex =  new Vector2 [] {new Vector2( -0.35955f*width, -0.47857f*height),
                new Vector2( +0.30337f*width, -0.50000f*height),
                new Vector2( +0.44944f*width, +0.00000f*height),
                new Vector2( -0.51685f*width, -0.01429f*height)};


        FixtureDef fixture = PhysicsFactory.createFixtureDef(DENSITY,ELASTICITY,FRICTION, false,mScene.CATEGORYBIT_PROJECTILES, mScene.MASKBITS_PROJECTILES, (short)0);
        mBody = PhysicsFactory.createPolygonBody(physicsWorld, mEntity, vertex, BodyDef.BodyType.DynamicBody, fixture);
        mBody.setAngularVelocity(angularVelocity);
        mBody.applyLinearImpulse(powerX * mBody.getMass(), powerY * mBody.getMass(), mBody.getWorldCenter().x, mBody.getWorldCenter().y);
        mBody.setUserData(this);

        return new PhysicsConnector(mEntity, mBody, true, true);
    }

    @Override
    public void destroy () {

        if (!mIsDestroyed){
            ResourcesMgr.getInstance().engine.runOnUpdateThread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < 6; i++) {
                        float scale = MathUtils.random(.35f,.8f);
                        ObjectBit bit = new ObjectBit(mEntity.getX() + (float) Math.random(), mEntity.getY() + (float) Math.random(), scale, mScene);
                        bit.mEntity.setColor(COLOR);
                        bit.mBody.setLinearVelocity(8 * (float) Math.random(), 8 * (float) Math.random());
                    }

                    mScene.unregisterUpdateHandler(Pot.this);
                    mPhysicsWorld.unregisterPhysicsConnector(mPhysicsConnector);
                    mPhysicsWorld.destroyBody(mBody);
                    mEntity.clearUpdateHandlers();
                    mEntity.clearEntityModifiers();
                    mEntity.detachSelf();
                    mEntity.dispose();
                    mEntity = null;
                    mBody = null;
                }
            });
            mIsDestroyed = true;
         }
    }


    @Override
    public void onBeginContact(Contact pContact) {
        //Log.d(Constants.TAG, "Bottle beginContact " + pContact.getFixtureA().getBody().getUserData()+ " with " + pContact.getFixtureB().getBody().getUserData());

        Fixture x1 = pContact.getFixtureA();
        Fixture x2 = pContact.getFixtureB();
        if ( ! (x1.getBody().getUserData() instanceof Pot)) {
            x2 = x1;
            x1 = pContact.getFixtureB();
        }
        assert (x1.getBody().getUserData() instanceof Pot);

        Pot pot = (Pot) x1.getBody().getUserData();

        if (x2.getBody().getUserData() instanceof Enemy) {

            /* Bottle - Zombie collision */
            Enemy enemy = (Enemy) x2.getBody().getUserData();
            if (pot.mBody.getLinearVelocity().len() > Constants.PROJECTILE_VELOCITY_TO_KILL) {

                // die monster die!
                enemy.hit(pot.mBody.getLinearVelocity().nor().mul(BASE_DAMAGE));
            }
        }

        ResourcesMgr.potCrashSound.setRate(MathUtils.random(.8f,1.2f));
        ResourcesMgr.potCrashSound.play();
        pot.destroy(); // Bottle explodes when contact with any object

    }

    @Override
    public void onEndContact(Contact pContact) {

    }

    @Override
    public void onPreSolve(Contact pContact, Manifold pOldManifold) {

    }

    @Override
    public void onPostSolve(Contact pContact, ContactImpulse pImpulse)  {

    }
}
