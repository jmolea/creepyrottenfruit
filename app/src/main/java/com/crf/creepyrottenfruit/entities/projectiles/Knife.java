package com.crf.creepyrottenfruit.entities.projectiles;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.entities.enemies.Enemy;
import com.crf.creepyrottenfruit.entities.PhysicalObject;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.util.adt.color.Color;
import org.andengine.util.math.MathUtils;

/*
 * Created by Jose Maria Olea on 10/11/2017.
 */

public class Knife extends Projectile implements IUpdateHandler {

    // ====================================================
    // CONSTANTS
    // ====================================================
    private static final float DENSITY = .8f;
    private static final float ELASTICITY = .02f;
    private static final float FRICTION = .65f;
    private final Color COLOR = new Color (13f/255,76f/255,40f/255);

    // ====================================================
    // VARIABLES
    // ====================================================

    private WeldJointDef weldJointDef;
    private float rotationDeviation;

    // ====================================================
    // CONSTRUCTOR
    // ====================================================
    public Knife(float x, float y, float powerX, float powerY, GameScene scene)  {
        mPhysicsWorld = scene.mPhysicsWorld;
        mScene = scene;

        mEntity = new TiledSprite(x, y, ResourcesMgr.cuttleryTexture, ResourcesMgr.getInstance().vertexBufferObjectManager);
        mEntity.setCullingEnabled(true);
        ((TiledSprite)mEntity).setCurrentTileIndex(MathUtils.random(4));

        mPhysicsConnector = createPhysics(mPhysicsWorld, powerX, powerY);

        // Put the projectile in our world
        mScene.getChildByIndex(Constants.LAYER_BACKGROUND).attachChild(mEntity);
        mPhysicsWorld.registerPhysicsConnector(mPhysicsConnector);
        rotationDeviation = MathUtils.random(-.35f,.35f);

        mEntity.registerUpdateHandler(this);
        scene.registerUpdateHandler(this);


    }

    public PhysicsConnector createPhysics(PhysicsWorld physicsWorld, float powerX, float powerY) {



        FixtureDef fixture = PhysicsFactory.createFixtureDef(DENSITY,ELASTICITY,FRICTION, false,mScene.CATEGORYBIT_KNIVES, mScene.MASKBITS_KNIVES, (short)0);
        mBody = PhysicsFactory.createBoxBody(physicsWorld, mEntity.getX() , mEntity.getY(), 20, 50, BodyDef.BodyType.DynamicBody, fixture);
        mBody.applyLinearImpulse(powerX * mBody.getMass(), powerY * mBody.getMass(), mBody.getWorldCenter().x, mBody.getWorldCenter().y);
        mBody.setUserData(this);


        return new PhysicsConnector(mEntity, mBody, true, true);
    }


    @Override
    public void onBeginContact(Contact pContact) {

        Fixture x1 = pContact.getFixtureA();
        Fixture x2 = pContact.getFixtureB();
        if ( ! (x1.getBody().getUserData() instanceof Knife)) {
            x2 = x1;
            x1 = pContact.getFixtureB();
        }
        assert (x1.getBody().getUserData() instanceof Knife);

        Knife knife = (Knife) x1.getBody().getUserData();

        /* Knife - enemy collision */
        if (x2.getBody().getUserData() instanceof Enemy) {

            Enemy enemy = (Enemy) x2.getBody().getUserData();
            // die monster die!
            enemy.hit(knife.mBody.getLinearVelocity().nor().mul(BASE_DAMAGE));
            knife.destroy();

        }
        /* Knife - world collision */
        else {

            /* Stick the knife to the target */
            PhysicalObject target  = (PhysicalObject) x2.getBody().getUserData();
            weldJointDef = new WeldJointDef();
            weldJointDef.initialize(knife.mBody, target.mBody, knife.mBody.getWorldCenter());
            ResourcesMgr.getInstance().activity.runOnUpdateThread(new Runnable() {
                @Override
                public void run() {
                    mPhysicsWorld.createJoint(weldJointDef);

                }
            });


            /* Fade out the knife */
            this.mHasExpiration = true;
            this.timeLeft = 5;
            this.fadeOutLength = 2;



        }


        ResourcesMgr.knifeHitSound.setRate(MathUtils.random(.8f,1.2f));
        ResourcesMgr.knifeHitSound.play();

    }

    @Override
    public void onEndContact(Contact pContact) {

    }

    @Override
    public void onPreSolve(Contact pContact, Manifold pOldManifold) {
        mBody.setTransform(mBody.getPosition().x, mBody.getPosition().y, (float)Math.PI);

    }

    @Override
    public void onPostSolve(Contact pContact, ContactImpulse pImpulse)  {

    }

    @Override
    public void onUpdate (float pSecondsElapsed) {
        if (!mIsDestroyed) {
            if (mHasExpiration) {
                timeLeft -= pSecondsElapsed;
                if (timeLeft < 0) {
                    this.destroy();
                } else if (timeLeft < fadeOutLength) {
                    mEntity.setAlpha(timeLeft / fadeOutLength);
                }
            } else {
            /* Keep the knife aligned to its trajectory */
                float angle = (float) (Math.atan2(mBody.getLinearVelocity().y, mBody.getLinearVelocity().x) - Math.PI /2 + rotationDeviation);
                mBody.setTransform(mBody.getPosition().x, mBody.getPosition().y, angle);
            }
        }
    }
}
