package com.crf.creepyrottenfruit.entities;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.sprite.batch.SpriteGroup;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.util.adt.color.Color;
import org.andengine.util.math.MathUtils;

import static org.andengine.extension.physics.box2d.util.constants.PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;

/*
 * Created by Jose Maria Olea on 10/11/2017.
 */

public class FruitBit extends PhysicalObject implements IUpdateHandler{

    // ====================================================
    // CONSTANTS
    // ====================================================
    private static final float DENSITY = .5f;
    private static final float ELASTICITY = .01f;
    private static final float FRICTION = .2f;

    // ====================================================
    // VARIABLES
    // ====================================================
    private Sprite spriteTop;

    // ====================================================
    // CONSTRUCTOR
    // ====================================================
    public FruitBit(float x, float y, float scaleFactor, Color colorBase, Color colorTop, GameScene scene) {
        mPhysicsWorld = scene.mPhysicsWorld;
        mScene = scene;

        int variation = (int) (Math.floor(Math.random()*2)); // 0 or 1
        mEntity =  new Sprite(0,0, ResourcesMgr.fruitBitBaseTexture [variation], ResourcesMgr.getInstance().vertexBufferObjectManager);

        spriteTop =  new Sprite(mEntity.getWidth()/2, mEntity.getHeight()/2, ResourcesMgr.fruitBitTopTexture [variation], ResourcesMgr.getInstance().vertexBufferObjectManager);
        mEntity.attachChild(spriteTop);

        mEntity.setColor(colorBase);
        spriteTop.setColor(colorTop);
        mEntity.setAlpha(.99f);
        final float scale = MathUtils.random(.8f,1.2f) * scaleFactor;
        mEntity.setScale(scale);
        mEntity.setPosition(x + (float)Math.random(),y + (float)Math.random());


        mPhysicsConnector = createPhysics(mPhysicsWorld, scale, variation);
        mPhysicsWorld.registerPhysicsConnector(mPhysicsConnector);

        this.mHasExpiration= true;
        this.timeLeft = 5;
        this.fadeOutLength = 2;

        ResourcesMgr.getInstance().engine.getScene().getChildByIndex(Constants.LAYER_CHARACTERS).attachChild(mEntity);
        scene.registerUpdateHandler(this);
    }

    public PhysicsConnector createPhysics(PhysicsWorld physicsWorld, float scale, int variation) {

        final float rotation = (float) (Math.PI * 2 * Math.random() );
        float width = scale * mEntity.getWidth()/PIXEL_TO_METER_RATIO_DEFAULT;
        float height = scale * mEntity.getHeight()/PIXEL_TO_METER_RATIO_DEFAULT;

        Vector2 [] vertex = null;
        switch (variation) {
            case 0:
                vertex = new Vector2 [] { new Vector2( -0.25000f*width, -0.18000f*height),
                        new Vector2( +0.23000f*width, -0.21000f*height),
                        new Vector2( +0.10000f*width, +0.31000f*height)};
                break;
            case 1:
                vertex = new Vector2 [] { new Vector2( -0.58000f*width, -0.19000f*height),
                        new Vector2( +0.49000f*width, -0.15000f*height),
                        new Vector2( +0.36000f*width, +0.38000f*height)};
                break;

        }

        FixtureDef fixture = PhysicsFactory.createFixtureDef(DENSITY,ELASTICITY,FRICTION, false,mScene.CATEGORYBIT_PROJECTILES, mScene.MASKBITS_PROJECTILES, (short)0);
        mBody = PhysicsFactory.createPolygonBody(physicsWorld, mEntity, vertex, BodyDef.BodyType.DynamicBody, fixture);
        mBody.setTransform(mBody.getPosition().x, mBody.getPosition().y, rotation );
        mBody.setUserData(this);

        return new PhysicsConnector(mEntity, mBody, true, true);

    }

    @Override
    public void onBeginContact(Contact pContact) {

    }

    @Override
    public void onEndContact(Contact pContact) {

    }

    @Override
    public void onPreSolve(Contact pContact, Manifold pOldManifold) {

    }

    @Override
    public void onPostSolve(Contact pContact, ContactImpulse pImpulse)  {

    }

    @Override
    public void onUpdate (float pSecondsElapsed) {
        if (mHasExpiration) {
            timeLeft -= pSecondsElapsed;
            if (timeLeft<0) {
                this.destroy();
            }
            else if (timeLeft < fadeOutLength) {
                float alpha = timeLeft/fadeOutLength;
                mEntity.setAlpha(alpha);
                spriteTop.setAlpha(alpha);
            }
        }
    }

    @Override
    public void reset() {

    }
}
