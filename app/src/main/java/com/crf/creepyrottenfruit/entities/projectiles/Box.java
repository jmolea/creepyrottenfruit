package com.crf.creepyrottenfruit.entities.projectiles;

import android.util.Log;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.entities.enemies.Enemy;
import com.crf.creepyrottenfruit.entities.ObjectBit;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.util.adt.color.Color;
import org.andengine.util.math.MathUtils;

/*
 * Created by Jose Maria Olea on 10/11/2017.
 */

public class Box extends Projectile{

    // ====================================================
    // CONSTANTS
    // ====================================================
    private static final float DENSITY = .8f;
    private static final float ELASTICITY = 0f;
    private static final float FRICTION = .95f;
    private static final Color COLOR = new Color (140f/255,98f/255,57f/255);

    // ====================================================
    // CONSTRUCTOR
    // ====================================================
    public Box(float x, float y, float powerX, float powerY, GameScene scene) {
        mPhysicsWorld = scene.mPhysicsWorld;
        mScene = scene;

        final float size = 50f + 70 * (float) Math.random(); // Between 50 and 120
        mEntity = new Sprite(x, y, (size > 90) ? ResourcesMgr.boxLargeTexture : ResourcesMgr.boxSmallTexture, ResourcesMgr.getInstance().vertexBufferObjectManager);
        mEntity.setCullingEnabled(true);
        mEntity.setWidth(size);
        mEntity.setHeight(size);

        mPhysicsConnector = createPhysics(mPhysicsWorld, powerX, powerY);

        // Put the projectile in our world
        mScene.getChildByIndex(Constants.LAYER_ITEMS).attachChild(mEntity);
        mPhysicsWorld.registerPhysicsConnector(mPhysicsConnector);

        mEntity.registerUpdateHandler(this);
    }

    public PhysicsConnector createPhysics(PhysicsWorld physicsWorld, float powerX, float powerY) {
        Log.d(Constants.TAG, "Box created with power "+powerX+","+powerY);

        final float angularVelocity = (float) (Math.PI * (Math.random() - .5d));

        FixtureDef fixture = PhysicsFactory.createFixtureDef(DENSITY,ELASTICITY,FRICTION, false, mScene.CATEGORYBIT_PROJECTILES, mScene.MASKBITS_PROJECTILES, (short)0);
        mBody = PhysicsFactory.createBoxBody(physicsWorld, mEntity.getX(), mEntity.getY(), mEntity.getWidth()-10, mEntity.getHeight()-10, BodyDef.BodyType.DynamicBody, fixture);
        mBody.setAngularVelocity(angularVelocity);
        mBody.applyLinearImpulse(powerX * mBody.getMass(), powerY * mBody.getMass(), mBody.getWorldCenter().x, mBody.getWorldCenter().y);
        mBody.setUserData(this);

        return new PhysicsConnector(mEntity, mBody, true, true);

    }

    @Override
    public void destroy () {

        if (!mIsDestroyed){
            ResourcesMgr.getInstance().engine.runOnUpdateThread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < 6; i++) {
                        float scale = .2f + .4f*(float) Math.random(); // Between .2 and .6
                        ObjectBit bit = new ObjectBit(mEntity.getX() + (float) Math.random(), mEntity.getY() + (float) Math.random(), scale, mScene);
                        bit.mEntity.setColor(COLOR);
                        Vector2 randomImpulse = new Vector2(8 * MathUtils.random(-1f,1f), 8 * MathUtils.random(-1f,1f));
                        bit.mBody.setLinearVelocity(mBody.getLinearVelocity().add(randomImpulse) );
                    }

                    mScene.unregisterUpdateHandler(Box.this);
                    mPhysicsWorld.unregisterPhysicsConnector(mPhysicsConnector);
                    mPhysicsWorld.destroyBody(mBody);
                    mEntity.clearUpdateHandlers();
                    mEntity.clearEntityModifiers();
                    mEntity.detachSelf();
                    mEntity.dispose();
                    mEntity = null;
                    mBody = null;
                }
            });
            mIsDestroyed = true;
        }
    }

    @Override
    public void onBeginContact(Contact pContact) {

        Fixture x1 = pContact.getFixtureA();
        Fixture x2 = pContact.getFixtureB();
        if ( ! (x1.getBody().getUserData() instanceof Box)) {
            x2 = x1;
            x1 = pContact.getFixtureB();
        }
        assert (x1.getBody().getUserData() instanceof Box);

        Box box = (Box) x1.getBody().getUserData();

        if (box.mBody.getLinearVelocity().len()>5 && !soundAlreadyPlaying) {
            //Log.d(Constants.TAG, "Box beginContact " + pContact.getFixtureA().getBody().getUserData()+ " with " + pContact.getFixtureB().getBody().getUserData() + " Speed: " + box.mBody.getLinearVelocity().len());
            ResourcesMgr.boxCrashSound.setRate(MathUtils.random(.8f,1.2f));
            ResourcesMgr.boxCrashSound.play();
            soundAlreadyPlaying = true;
            ResourcesMgr.getInstance().engine.registerUpdateHandler(new TimerHandler(.5f, new ITimerCallback() {
                @Override
                public void onTimePassed(TimerHandler pTimerHandler) {
                    ResourcesMgr.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                    if (!mIsDestroyed) {
                        soundAlreadyPlaying = false;
                    }
                }
            }));
        }

        if (x2.getBody().getUserData() instanceof Enemy) {

            /* Box - Zombie collision */
            Enemy enemy = (Enemy) x2.getBody().getUserData();
            // die monster die
            enemy.hit(box.mBody.getLinearVelocity().mul(box.mBody.getMass()));
        }


    }

    @Override
    public void onEndContact(Contact pContact) {

    }

    @Override
    public void onPreSolve(Contact pContact, Manifold pOldManifold) {

    }

    @Override
    public void onPostSolve(Contact pContact, ContactImpulse pImpulse) {

    }

}
