package com.crf.creepyrottenfruit.entities.projectiles;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.util.math.MathUtils;

/*
 * Created by Jose Maria Olea on 10/11/2017.
 */

public class Ball extends Projectile{

    // ====================================================
    // CONSTANTS
    // ====================================================
    private static final float DENSITY = .2f;
    private static final float ELASTICITY = .8f;
    private static final float FRICTION = .6f;

    // ====================================================
    // CONSTRUCTOR
    // ====================================================
    public Ball(float x, float y, float powerX, float powerY, GameScene scene) {
        mPhysicsWorld = scene.mPhysicsWorld;
        mScene = scene;

        final float size = 100;
        mEntity = new Sprite(x, y, ResourcesMgr.ballTexture, ResourcesMgr.getInstance().vertexBufferObjectManager);
        mEntity.setCullingEnabled(true);
        mEntity.setHeight(size);
        mEntity.setWidth(size);


        mPhysicsConnector =  createPhysics(mPhysicsWorld,  powerX,  powerY);

        // Put the projectile in our world
        mScene.getChildByIndex(Constants.LAYER_ITEMS).attachChild(mEntity);
        mPhysicsWorld.registerPhysicsConnector(mPhysicsConnector);

        //this.mEntity.registerUpdateHandler(this);

    }

    public PhysicsConnector createPhysics(PhysicsWorld physicsWorld, float powerX, float powerY) {

        final float angularVelocity = (float) (Math.PI * (Math.random() - .5d));


        FixtureDef fixture = PhysicsFactory.createFixtureDef(DENSITY,ELASTICITY,FRICTION, false,mScene.CATEGORYBIT_PROJECTILES, mScene.MASKBITS_PROJECTILES, (short)0);
        mBody = PhysicsFactory.createCircleBody(physicsWorld, mEntity.getX(), mEntity.getY(), (mEntity.getHeight()/2)-5, mEntity.getHeight()-10, BodyDef.BodyType.DynamicBody, fixture);
        mBody.setAngularVelocity(angularVelocity);
        mBody.applyLinearImpulse(powerX * mBody.getMass(), powerY * mBody.getMass(), mBody.getWorldCenter().x, mBody.getWorldCenter().y);
        mBody.setUserData(this);

        return new PhysicsConnector(mEntity, mBody, true, true);
    }

    @Override
    public void onBeginContact(Contact pContact) {
        Fixture x1 = pContact.getFixtureA();
        if ( ! (x1.getBody().getUserData() instanceof Ball)) {
            x1 = pContact.getFixtureB();
        }
        assert (x1.getBody().getUserData() instanceof Ball);

        Ball ball = (Ball) x1.getBody().getUserData();

        if (ball.mBody.getLinearVelocity().len()>5 && !soundAlreadyPlaying) {
            ResourcesMgr.ballHitSound.setRate(MathUtils.random(.8f,1.2f));
            ResourcesMgr.ballHitSound.play();
            soundAlreadyPlaying = true;
            ResourcesMgr.getInstance().engine.registerUpdateHandler(new TimerHandler(.5f, new ITimerCallback() {
                @Override
                public void onTimePassed(TimerHandler pTimerHandler) {
                    ResourcesMgr.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                    if (!mIsDestroyed) {
                        soundAlreadyPlaying = false;
                    }
                }
            }));
        }
    }

    @Override
    public void onEndContact(Contact pContact) {

    }

    @Override
    public void onPreSolve(Contact pContact, Manifold pOldManifold) {

    }

    @Override
    public void onPostSolve(Contact pContact, ContactImpulse pImpulse)  {

    }
}
