package com.crf.creepyrottenfruit.entities;

import android.content.Context;
import android.os.Vibrator;
import android.util.Log;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.entities.projectiles.*;
import com.crf.creepyrottenfruit.hud.Item;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.util.math.MathUtils;

import static org.andengine.extension.physics.box2d.util.constants.PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;

/*
 * Created by Jose Maria Olea on 03/01/2018.
 */

public class Onagre extends PhysicalObject {

    // ====================================================
    // CONSTANTS
    // ====================================================

    private final float SHOOT_TIME = 0f;
    private final float RELOAD_TIME = 2.5f;//2F;

    public final float LAUNCH_POSITION_OFFSET_X = -195;
    public final float LAUNCH_POSITION_OFFSET_Y = 55;
    public final float SHOOTING_ANGLE_RAD =  50 *  (float)Math.PI / 180;
    public final float SHOOTING_ANGLE_COS = (float) Math.cos(SHOOTING_ANGLE_RAD);
    public final float SHOOTING_ANGLE_SIN = (float) Math.sin(SHOOTING_ANGLE_RAD);

    public final float BOX_CHANCE = .65f;
    public final float POT_CHANCE = BOX_CHANCE + .10f;
    public final float BOTTLE_CHANCE = POT_CHANCE + .14f;
    public final float WATERINGCAN_CHANCE = BOTTLE_CHANCE + .08f;
    public final float BALL_CHANCE = WATERINGCAN_CHANCE + .03f;


    public final float LAUNCH_MIN_POWER = 220 * 0.05f;
    public final float LAUNCH_MAX_POWER = 580 * 0.05f;
    public final float LAUNCH_MIN_MAX_RANGE = LAUNCH_MAX_POWER - LAUNCH_MIN_POWER;

    protected static final float DENSITY = .5f;
    protected static final float ELASTICITY = .5f;
    protected static final float FRICTION =1f;



    // ====================================================
    // VARIABLES
    // ====================================================

    public OnagrePart body, arm, wheel, tireLeft, tireRight;
    private State currentState;
    private float mPower;
    private boolean mShootWhenReady;


    enum State {FIRING, RELOADING, READY};


    // ====================================================
    // CONSTRUCTOR
    // ====================================================
    public Onagre(float x, float y, final GameScene scene) {
        mPhysicsWorld = scene.mPhysicsWorld;
        mScene = scene;

        body = new OnagrePart();
        arm = new OnagrePart();
        wheel = new OnagrePart();
        tireLeft = new OnagrePart();
        tireRight = new OnagrePart();


        body.mSprite = new Sprite(x, y, ResourcesMgr.onagreBody, ResourcesMgr.getInstance().vertexBufferObjectManager);
        //body.mSprite.setAnchorCenter(0.0f, 0);

        arm.mSprite = new Sprite(x + 61, y + 38, ResourcesMgr.onagreArm, ResourcesMgr.getInstance().vertexBufferObjectManager);
        arm.mSprite.setAnchorCenter(247 / arm.mSprite.getWidth(), 51 / arm.mSprite.getHeight());

        wheel.mSprite = new Sprite(x -30, y -56, ResourcesMgr.onagreWheel, ResourcesMgr.getInstance().vertexBufferObjectManager);
        wheel.mSprite.setAnchorCenter(32 / wheel.mSprite.getWidth(), 35 / wheel.mSprite.getHeight());

        tireLeft.mSprite = new Sprite(x -129, y -88, ResourcesMgr.onagreTire, ResourcesMgr.getInstance().vertexBufferObjectManager);
        tireLeft.mSprite.setAnchorCenter(54 / tireLeft.mSprite.getWidth(), 54 / tireLeft.mSprite.getHeight());

        tireRight.mSprite= new Sprite(x + 133, y -88, ResourcesMgr.onagreTire, ResourcesMgr.getInstance().vertexBufferObjectManager);
        tireRight.mSprite.setAnchorCenter(54 / tireRight.mSprite.getWidth(), 54 / tireRight.mSprite.getHeight());

        currentState = State.READY;

        scene.getChildByIndex(Constants.LAYER_CHARACTERS).attachChild(arm.mSprite);
        scene.getChildByIndex(Constants.LAYER_CHARACTERS).attachChild(body.mSprite);
        scene.getChildByIndex(Constants.LAYER_CHARACTERS).attachChild(wheel.mSprite);
        scene.getChildByIndex(Constants.LAYER_CHARACTERS).attachChild(tireLeft.mSprite);
        scene.getChildByIndex(Constants.LAYER_CHARACTERS).attachChild(tireRight.mSprite);

        createPhysics(mPhysicsWorld);


        this.mScene.registerUpdateHandler(new IUpdateHandler() {
            @Override
            public void onUpdate(float pSecondsElapsed) {
                if (scene.gameState == GameScene.GameState.PLAYING) {
                    switch (currentState) {
                        case READY:
                            if (mShootWhenReady) {
                                shoot();
                            }
                            break;
                        case FIRING:
                            break;
                        case RELOADING:
                            wheel.mBody.setAngularVelocity(1f);
                    }
                }
            }

            @Override
            public void reset() {

            }
        });
    }

    // ====================================================
    // METHODS
    // ====================================================

    private void createPhysics (PhysicsWorld physicsWorld) {

        /* Create body */
        FixtureDef fixture = PhysicsFactory.createFixtureDef(0f,ELASTICITY,FRICTION, false, GameScene.CATEGORYBIT_ONAGRE, GameScene.MASKBITS_ONAGRE, (short)0);
        body.mBody = PhysicsFactory.createBoxBody(mPhysicsWorld, body.mSprite, BodyDef.BodyType.KinematicBody, fixture);
        mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(body.mSprite, body.mBody, true, true));

        fixture = PhysicsFactory.createFixtureDef(.2f,ELASTICITY,FRICTION, false, GameScene.CATEGORYBIT_ONAGRE, GameScene.MASKBITS_ONAGRE, (short)0);
        arm.mBody = PhysicsFactory.createBoxBody(mPhysicsWorld, arm.mSprite, BodyDef.BodyType.DynamicBody, fixture);
        mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(arm.mSprite, arm.mBody, true, true));

        fixture = PhysicsFactory.createFixtureDef(.5f,ELASTICITY,FRICTION, false, GameScene.CATEGORYBIT_ONAGRE, GameScene.MASKBITS_ONAGRE, (short)0);
        wheel.mBody = PhysicsFactory.createCircleBody(mPhysicsWorld, wheel.mSprite, BodyDef.BodyType.DynamicBody, fixture);
        mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(wheel.mSprite, wheel.mBody, true, true));

        tireLeft.mBody = PhysicsFactory.createCircleBody(mPhysicsWorld, tireLeft.mSprite, BodyDef.BodyType.DynamicBody, fixture);
        mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(tireLeft.mSprite, tireLeft.mBody, true, true));

        tireRight.mBody = PhysicsFactory.createCircleBody(mPhysicsWorld, tireRight.mSprite, BodyDef.BodyType.DynamicBody, fixture);
        mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(tireRight.mSprite, tireRight.mBody, true, true));


        /* Create joints */

        // Arm
        RevoluteJointDef revoluteJointDef = new RevoluteJointDef();
        Vector2 center = body.mBody.getWorldCenter();
        center.add(61/PIXEL_TO_METER_RATIO_DEFAULT, 38/PIXEL_TO_METER_RATIO_DEFAULT);
        revoluteJointDef.initialize(body.mBody, arm.mBody, center);
        revoluteJointDef.enableLimit = true;
        revoluteJointDef.lowerAngle = - 45 * (float) Math.PI/180;
        revoluteJointDef.upperAngle = 10 * (float) Math.PI/180;
        mPhysicsWorld.createJoint(revoluteJointDef);

        // Wheel
        revoluteJointDef = new RevoluteJointDef();
        revoluteJointDef.initialize(body.mBody, wheel.mBody, wheel.mBody.getWorldCenter());
        revoluteJointDef.enableLimit = true;
        revoluteJointDef.lowerAngle = - 360 * (float) Math.PI/180;
        revoluteJointDef.upperAngle = 360 * (float) Math.PI/180;
        mPhysicsWorld.createJoint(revoluteJointDef);

        //Tire left
        revoluteJointDef = new RevoluteJointDef();
        revoluteJointDef.initialize(body.mBody, tireLeft.mBody, tireLeft.mBody.getWorldCenter());
        revoluteJointDef.enableLimit = true;
        revoluteJointDef.lowerAngle = - 10 * (float) Math.PI/180;
        revoluteJointDef.upperAngle = 10 * (float) Math.PI/180;
        mPhysicsWorld.createJoint(revoluteJointDef);

        //Tire Right
        revoluteJointDef = new RevoluteJointDef();
        revoluteJointDef.initialize(body.mBody, tireRight.mBody, tireRight.mBody.getWorldCenter());
        mPhysicsWorld.createJoint(revoluteJointDef);

    }


    public void shootWhenReady(float pSliderPower) {
        Log.d(Constants.TAG, "Shoot button pressed");
        mPower = LAUNCH_MIN_POWER + (pSliderPower * (LAUNCH_MAX_POWER - LAUNCH_MIN_POWER));
        mShootWhenReady = true;
    }

    public void shoot () {
        currentState = State.FIRING;
        mShootWhenReady = false;

        // Animate bodies
        arm.mBody.setAngularVelocity(- mPower * .2f );
        wheel.mBody.setAngularVelocity(-10f);

        Vibrator v = (Vibrator) ResourcesMgr.getInstance().activity.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        v.vibrate(100);
        ResourcesMgr.thumpSound.setRate(MathUtils.random(.9f,1.1f));
        ResourcesMgr.thumpSound.setVolume (.5f);
        ResourcesMgr.thumpSound.play();

        createProjectile();
        reload();
    }

    private void createProjectile() {

        // Create  inside runOnUpdateThread, so wont create the body during physics calculations
        ResourcesMgr.getInstance().engine.runOnUpdateThread(new Runnable() {
            @Override
            public void run() {
                Log.d(Constants.TAG, "Creating projectil");

                float powerX = SHOOTING_ANGLE_COS * mPower;
                float powerY = SHOOTING_ANGLE_SIN * mPower;

                if (mScene.itemsBar.getSelectedType() == Item.Type.CUTTLERY) {
                    new Knife(body.mSprite.getX() + LAUNCH_POSITION_OFFSET_X, body.mSprite.getY() + LAUNCH_POSITION_OFFSET_Y, powerX - MathUtils.random(1f, 3f), powerY - MathUtils.random(.5f, 1.5f), mScene);
                    new Knife(body.mSprite.getX() + LAUNCH_POSITION_OFFSET_X, body.mSprite.getY() + LAUNCH_POSITION_OFFSET_Y, powerX - MathUtils.random(.5f, 1.5f), powerY - MathUtils.random(.5f, 1.5f), mScene);
                    new Knife(body.mSprite.getX() + LAUNCH_POSITION_OFFSET_X, body.mSprite.getY() + LAUNCH_POSITION_OFFSET_Y, powerX, powerY, mScene);
                    new Knife(body.mSprite.getX() + LAUNCH_POSITION_OFFSET_X, body.mSprite.getY() + LAUNCH_POSITION_OFFSET_Y, powerX + MathUtils.random(.5f, 1.5f), powerY + MathUtils.random(.5f, 1.5f), mScene);
                    new Knife(body.mSprite.getX() + LAUNCH_POSITION_OFFSET_X, body.mSprite.getY() + LAUNCH_POSITION_OFFSET_Y, powerX - MathUtils.random(1f, 3f), powerY - MathUtils.random(.5f, 1.5f), mScene);
                    mScene.itemsBar.removeSelectedItem();
                } else if (mScene.itemsBar.getSelectedType() == Item.Type.DYNAMITE) {
                    new Dynamite(body.mSprite.getX() + LAUNCH_POSITION_OFFSET_X, body.mSprite.getY() + LAUNCH_POSITION_OFFSET_Y, powerX, powerY, mScene);
                    mScene.itemsBar.removeSelectedItem();
                } else if (mScene.itemsBar.getSelectedType() == Item.Type.CLOCK) {
                    new Clock(body.mSprite.getX() + LAUNCH_POSITION_OFFSET_X, body.mSprite.getY() + LAUNCH_POSITION_OFFSET_Y, powerX, powerY, mScene);
                    mScene.itemsBar.removeSelectedItem();
                } else {
                    float random = (float) Math.random();

                    if (random <= BOX_CHANCE) {
                        new Box(body.mSprite.getX() + LAUNCH_POSITION_OFFSET_X, body.mSprite.getY() + LAUNCH_POSITION_OFFSET_Y, powerX, powerY, mScene);
                    } else if (random <= POT_CHANCE) {
                        new Pot(body.mSprite.getX() + LAUNCH_POSITION_OFFSET_X, body.mSprite.getY() + LAUNCH_POSITION_OFFSET_Y, powerX, powerY, mScene);
                    } else if (random <= BOTTLE_CHANCE) {
                        new Bottle(body.mSprite.getX() + LAUNCH_POSITION_OFFSET_X, body.mSprite.getY() + LAUNCH_POSITION_OFFSET_Y, powerX, powerY, mScene);
                    } else if (random <= WATERINGCAN_CHANCE) {
                        new WateringCan(body.mSprite.getX() + LAUNCH_POSITION_OFFSET_X, body.mSprite.getY() + LAUNCH_POSITION_OFFSET_Y, powerX, powerY, mScene);
                    } else {
                        new Ball(body.mSprite.getX() + LAUNCH_POSITION_OFFSET_X, body.mSprite.getY() + LAUNCH_POSITION_OFFSET_Y, powerX, powerY, mScene);
                    }
                }
            }
        });
    }

    private void reload() {
        currentState = State.RELOADING;

        ResourcesMgr.getInstance().engine.registerUpdateHandler(new TimerHandler(RELOAD_TIME, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                Log.d(Constants.TAG, "Onagre ready");
                ResourcesMgr.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                currentState = State.READY;
            }
        }));
    }

    @Override
    public void onBeginContact(Contact pContact) {

    }

    @Override
    public void onEndContact(Contact pContact) {

    }

    @Override
    public void onPreSolve(Contact pContact, Manifold pOldManifold) {

    }

    @Override
    public void onPostSolve(Contact pContact, ContactImpulse pImpulse)  {

    }

    public class OnagrePart {
        public Sprite mSprite;
        public Body mBody;

        public OnagrePart () {
        }

        public OnagrePart (Sprite sprite, Body body) {
            mSprite = sprite;
            mBody = body;
        }

    }
}
