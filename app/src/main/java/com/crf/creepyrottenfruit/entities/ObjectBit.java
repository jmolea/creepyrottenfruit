package com.crf.creepyrottenfruit.entities;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;

import static org.andengine.extension.physics.box2d.util.constants.PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;

/*
 * Created by Jose Maria Olea on 10/11/2017.
 */

public class ObjectBit extends PhysicalObject implements IUpdateHandler {

    // ====================================================
    // CONSTANTS
    // ====================================================
    private static final float DENSITY = .5f;
    private static final float ELASTICITY = .5f;
    private static final float FRICTION = .1f;

    // ====================================================
    // CONSTRUCTOR
    // ====================================================

    public ObjectBit(float x, float y, float scale, GameScene scene) {
        mPhysicsWorld = scene.mPhysicsWorld;
        mScene = scene;

        mEntity =  new Sprite(x + (float)Math.random(),y + (float)Math.random(), ResourcesMgr.objectBitTexture, ResourcesMgr.getInstance().vertexBufferObjectManager);
        mEntity.setScale(scale);

        mPhysicsConnector = createPhysics(mPhysicsWorld, scale);
        mPhysicsWorld.registerPhysicsConnector(mPhysicsConnector);

        this.mHasExpiration= true;
        this.timeLeft = 5;
        this.fadeOutLength = 2;

        ResourcesMgr.getInstance().engine.getScene().getChildByIndex(Constants.LAYER_CHARACTERS).attachChild(mEntity);
        scene.registerUpdateHandler(this);

    }

    public PhysicsConnector createPhysics(PhysicsWorld physicsWorld, float scale) {

        final float rotation = (float) (Math.PI * 2 * Math.random() );
        float width = scale * mEntity.getWidth()/PIXEL_TO_METER_RATIO_DEFAULT;
        float height = scale * mEntity.getHeight()/PIXEL_TO_METER_RATIO_DEFAULT;

        Vector2 [] vertex =  new Vector2 [] {new Vector2( -0.36000f*width, -0.36000f*height),
                new Vector2( +0.44000f*width, -0.36000f*height),
                new Vector2( +0.30000f*width, +0.38000f*height)};


        FixtureDef fixture = PhysicsFactory.createFixtureDef(DENSITY,ELASTICITY,FRICTION, false,mScene.CATEGORYBIT_PROJECTILES, mScene.MASKBITS_PROJECTILES, (short)0);
        mBody = PhysicsFactory.createPolygonBody(physicsWorld, mEntity, vertex, BodyDef.BodyType.DynamicBody, fixture);
        mBody.setTransform(mBody.getPosition().x, mBody.getPosition().y, rotation );
        mBody.setUserData(this);

        return new PhysicsConnector(mEntity, mBody, true, true);
    }


    @Override
    public void onBeginContact(Contact pContact) {

    }

    @Override
    public void onEndContact(Contact pContact) {

    }

    @Override
    public void onPreSolve(Contact pContact, Manifold pOldManifold) {

    }

    @Override
    public void onPostSolve(Contact pContact, ContactImpulse pImpulse)  {

    }

    @Override
    public void onUpdate (float pSecondsElapsed) {
        if (mHasExpiration) {
            timeLeft -= pSecondsElapsed;
            if (timeLeft<0) {
                this.destroy();
            }
            else if (timeLeft < fadeOutLength) {
                mEntity.setAlpha(timeLeft/fadeOutLength);
            }


        }
    }

}
