package com.crf.creepyrottenfruit.entities.enemies;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.entities.FruitBit;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.util.adt.color.Color;
import org.andengine.util.math.MathUtils;

import static org.andengine.extension.physics.box2d.util.constants.PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;

/*
 * Created by Jose Maria Olea on 07/01/2018.
 */

public class ZombieGrape extends Enemy implements IUpdateHandler {
    // ====================================================
    // CONSTANTS
    // ====================================================
    //private static float JUMP_POWER = 8f;
    private static float SPEED = 1.5f;
    private static float LIFE_POINTS =50;
    private static float MIN_SPAWN_ALTITUDE = 400;
    private static float MAX_SPAWN_ALTITUDE = 600;
    private static float OSCILLATION_HEIGHT = 20;
    private static float CEILING_ALTITUDE = 700;
    private static final Color COLOR = new Color (176f/255,130f/255,176f/255);
    private static final Color COLOR_OUT =  new Color (122f/255,84f/255,122f/255);
    private static final float BIT_SCALE_FACTOR = .3f;


    // ====================================================
    // VARIABLES
    // ====================================================

    protected float altitude;

    // ====================================================
    // CONSTRUCTOR
    // ====================================================

    public ZombieGrape(float x, float y, GameScene scene) {
        mPhysicsWorld = scene.mPhysicsWorld;
        mScene = scene;

        altitude = MathUtils.random(MIN_SPAWN_ALTITUDE, MAX_SPAWN_ALTITUDE);
        mEntity = new AnimatedSprite(x, altitude, ResourcesMgr.zombieGrapeTextureRegion, ResourcesMgr.getInstance().vertexBufferObjectManager) ;
        mEntity.setCullingEnabled(true);
        ((AnimatedSprite)mEntity).animate(new long[]{90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90}, 0, 15, true);

        mPhysicsConnector = createPhysics(mPhysicsWorld);

        // Put the zombie in our world
        mScene.getChildByIndex(Constants.LAYER_CHARACTERS).attachChild(mEntity);
        mPhysicsWorld.registerPhysicsConnector(mPhysicsConnector);


        this.mEntity.registerUpdateHandler(this);

        lifePoints = this.LIFE_POINTS;
        speed = this.SPEED;
        speed2 = SPEED * SPEED;
        color = this.COLOR;
        soundPitchFactor = 1.5f;

    }
    // ====================================================
    // METHODS
    // ====================================================

    private PhysicsConnector createPhysics (PhysicsWorld physicsWorld) {

        FixtureDef fixture = PhysicsFactory.createFixtureDef(DENSITY,ELASTICITY,FRICTION, false, mScene.CATEGORYBIT_ENEMIES, mScene.MASKBITS_ENEMIES, (short)0);
        float width = mEntity.getWidth()/PIXEL_TO_METER_RATIO_DEFAULT;
        float height = mEntity.getHeight()/PIXEL_TO_METER_RATIO_DEFAULT;

        Vector2[] vertex = new Vector2[]{new Vector2( -0.48684f*width, -0.11811f*height),
                new Vector2( -0.06579f*width, -0.40157f*height),
                new Vector2( +0.34211f*width, -0.14961f*height),
                new Vector2( +0.23684f*width, +0.23622f*height),
                new Vector2( -0.17105f*width, +0.25197f*height)
        };

        mBody = PhysicsFactory.createPolygonBody(physicsWorld, mEntity, vertex, BodyDef.BodyType.DynamicBody, fixture);
        mBody.setUserData(this);

        //initial Y speed
        //mBody.setLinearVelocity(-30,-.5f);



        return new PhysicsConnector(mEntity, mBody, true, true);
    }

    @Override
    public void animate (Animation animation) {
        if (mEntity != null) {
            switch (animation) {
                case STUNNED:
                case WALKING:
                    ((AnimatedSprite) mEntity).animate(new long[]{90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90}, 0, 15, true);
                    break;

                    //((AnimatedSprite) mEntity).stopAnimation(16);
            }
        }
    }

    @Override
    public void destroy () {

            if (!mIsDestroyed) {

                //Splash
                mScene.splashManager.createSplash(mEntity.getX(), mEntity.getY(), this.color);


            ResourcesMgr.getInstance().engine.runOnUpdateThread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < AMOUNT_OF_PIECES_WHEN_DESTROYED; i++) {

                        FruitBit bit = new FruitBit(mEntity.getX() + (float) Math.random(), mEntity.getY() + (float) Math.random(), BIT_SCALE_FACTOR, COLOR, COLOR_OUT, mScene);
                            //if (impulse != null) {
                                Vector2 randomImpulse = new Vector2(MathUtils.random(-10f,10f), MathUtils.random(-10f,10f));
                                bit.mBody.setLinearVelocity(mBody.getLinearVelocity().add(randomImpulse) );
                            //}
                        }

                        mScene.unregisterUpdateHandler(ZombieGrape.this);
                        mPhysicsWorld.unregisterPhysicsConnector(mPhysicsConnector);
                        mPhysicsWorld.destroyBody(mBody);
                        mEntity.clearUpdateHandlers();
                        mEntity.clearEntityModifiers();
                        mEntity.detachSelf();
                        mEntity.dispose();
                        mEntity = null;
                        mBody = null;

                    }
                });

                mIsDestroyed = true;
                mScene.enemiesLeft --;
            }

    }

    @Override
    public void onUpdate (float pSecondesElapsed){
        callPhysicObjectUpdate(pSecondesElapsed);

        // Flying force counters gravity
        mBody.applyForce(new Vector2(0,10f).mul(mBody.getMass()), mBody.getWorldCenter());

        if (mScene.ringinClock != null && lured == false) { // Start ringing
            lured = true;
            pause (PAUSED_TIME * MathUtils.random(.8f,1.2f));

        }
        else if (mScene.ringinClock == null && lured == true) { // Stop ringing
            lured = false;
            pause (PAUSED_TIME * MathUtils.random(.8f,1.2f));
        }

        if (lured) {
            // Walk to the clock
            direction =   mScene.ringinClock.mBody.getPosition().x - mBody.getPosition().x ;
            if (Math.abs(direction) < 1) {
                pause (1f);
            } else {
                direction = Math.signum(direction);
            }
        } else {
            direction = -1;
        }


        switch (state) {
            case PAUSED:
                mBody.setTransform(mBody.getPosition().x, mBody.getPosition().y, 0);
                lastSpeed2 = 0;
                break;

            case WALKING:
                // Face walking direction
                ((Sprite)mEntity).setFlippedHorizontal( direction > 0);

                // Always standing up
                mBody.setTransform(mBody.getPosition().x, mBody.getPosition().y, 0);

                float currentSpeed = Math.abs(mBody.getLinearVelocity().x);

                if (currentSpeed < speed) { // Too slow
                    mBody.setLinearVelocity(mBody.getLinearVelocity().x  + direction*.01f,mBody.getLinearVelocity().y);
                }
                else if (currentSpeed >= speed) { // Too fast
                    mBody.setLinearVelocity(speed * direction, mBody.getLinearVelocity().y);
                }

                // Vertical speed
                if (mBody.getPosition().y * PIXEL_TO_METER_RATIO_DEFAULT > altitude ) {
                    mBody.setLinearVelocity(mBody.getLinearVelocity().x , mBody.getLinearVelocity().y - 0.05f);
                    if (mBody.getPosition().y * PIXEL_TO_METER_RATIO_DEFAULT > CEILING_ALTITUDE){
                        mBody.setLinearVelocity(mBody.getLinearVelocity().x , 0);
                        mBody.setTransform(mBody.getPosition().x, CEILING_ALTITUDE / PIXEL_TO_METER_RATIO_DEFAULT, mBody.getAngle());
                    }
                }
                else if (mBody.getPosition().y * PIXEL_TO_METER_RATIO_DEFAULT < altitude ) {
                    mBody.setLinearVelocity(mBody.getLinearVelocity().x, mBody.getLinearVelocity().y + 0.05f);
                }

                lastSpeed2 = currentSpeed;
                break;

            case RECOVERING:
                float angle = mBody.getAngle() % (2 * (float) Math.PI);
                if (angle > .2f) {
                    mBody.setTransform(mBody.getPosition().x, mBody.getPosition().y, angle - .2f);
                } else if (angle < -.2f) {
                    mBody.setTransform(mBody.getPosition().x, mBody.getPosition().y, angle + .2f);
                } else {
                    mBody.setTransform(mBody.getPosition().x, mBody.getPosition().y, 0);
                    state = EnemyState.WALKING;
                }
                lastSpeed2 = 0;
                break;

        }
    }

}
