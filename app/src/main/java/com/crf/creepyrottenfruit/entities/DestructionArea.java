package com.crf.creepyrottenfruit.entities;

import android.util.Log;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.util.adt.color.Color;

/*
 * Created by Jose Maria Olea on 06/01/2018.
 */

public class DestructionArea extends PhysicalObject {


    /** Default fixture (destroy projectiles and enemies) */
    public DestructionArea(float x, float y, float w, float h, GameScene scene) {
        FixtureDef fixture = PhysicsFactory.createFixtureDef(0,0,0, true, mScene.CATEGORYBIT_PROJECTILES, mScene.MASKBITS_PROJECTILES, (short)0);
        new DestructionArea (x,y,w,h,scene, fixture);
    }

    public DestructionArea(float x, float y, float w, float h,  GameScene scene, FixtureDef fixture) {
        mPhysicsWorld = scene.mPhysicsWorld;
        mScene = scene;

        mEntity = new Rectangle(x,y,w,h, ResourcesMgr.getInstance().vertexBufferObjectManager);
        mEntity.setColor(Color.BLACK);
        mScene.getChildByIndex(Constants.LAYER_EFFECTS).attachChild(mEntity);
        if (!Constants.DEBUG_MODE) {
            mEntity.setVisible(false);
        }

        mBody = PhysicsFactory.createBoxBody(mPhysicsWorld, x,y,w,h, BodyDef.BodyType.StaticBody, fixture);
        mBody.setUserData(this);

        mEntity.registerUpdateHandler(this);
    }

    @Override
    public void onBeginContact(Contact pContact) {
        //Log.d(Constants.TAG, "DestructionArea beginContact " + pContact.getFixtureA().getBody().getUserData()+ " with " + pContact.getFixtureB().getBody().getUserData());


        Fixture x1 = pContact.getFixtureA();
        Fixture x2 = pContact.getFixtureB();
        if ( ! (x1.getBody().getUserData() instanceof DestructionArea)) {
            x2 = x1;
            x1 = pContact.getFixtureB();
        }
        assert (x1.getBody().getUserData() instanceof DestructionArea);

        /* Object enter destruction area */
        PhysicalObject po = (PhysicalObject)  x2.getBody().getUserData();
        po.destroy();

}

    @Override
    public void onEndContact(Contact pContact) {

    }

    @Override
    public void onPreSolve(Contact pContact, Manifold pOldManifold) {

    }

    @Override
    public void onPostSolve(Contact pContact, ContactImpulse pImpulse)  {

    }
    // ====================================================
    // CONSTANTS
    // ====================================================

    // ====================================================
    // VARIABLES
    // ====================================================

    // ====================================================
    // CONSTRUCTOR
    // ====================================================

    // ====================================================
    // METHODS
    // ====================================================
}
