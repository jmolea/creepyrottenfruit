package com.crf.creepyrottenfruit.entities.enemies;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.entities.PhysicalObject;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.util.adt.color.Color;
import org.andengine.util.math.MathUtils;


/*
 * Created by Jose Maria Olea on 29/12/2017.
 */

public class Enemy extends PhysicalObject {

    // ====================================================
    // CONSTANTS
    // ====================================================
    public enum EnemyState {
        WALKING,
        STUNNED,
        RECOVERING,
        PAUSED,
        SPAWNING
    }

    public enum Animation {
        WALKING,
        STUNNED,
        SPAWNING
    }

    protected static final float DENSITY = .5f;
    protected static final float ELASTICITY = .2f;
    protected static final float FRICTION = .5f;
    protected static final float STUNNED_TIME = .75f;
    protected static final float PAUSED_TIME = 1f;
    protected static final int AMOUNT_OF_PIECES_WHEN_DESTROYED = 5;


    // ====================================================
    // VARIABLES
    // ====================================================
    //protected Vector2 impulse;
    protected EnemyState state = EnemyState.SPAWNING;

    protected float lifePoints;
    protected float direction = -1;
    protected boolean lured = false;
    protected float lastSpeed2 =0f;
    protected float speed, jumpPower;
    protected float speed2, currentSpeed2;
    protected Color color;
    protected boolean soundAlreadyPlaying = false;
    protected float soundPitchFactor =1f;


    // ====================================================
    // CONSTRUCTOR
    // ====================================================

    public Enemy () {

    }

    // ====================================================
    // METHODS
    // ====================================================


//    public void destroy (Vector2 impulse) {
//        this.impulse = impulse;
//        destroy();
//    }

    public void hit (Vector2 impulse) {
        //Log.d(Constants.TAG, "Hit with impulse = " + impulse.len());

        if (impulse.len()>10) {

            if (!soundAlreadyPlaying) {
                ResourcesMgr.zombieMoan1Sound.setRate(MathUtils.random(.8f * soundPitchFactor, 1.2f * soundPitchFactor));
                ResourcesMgr.zombieMoan1Sound.play();
                soundAlreadyPlaying = true;

                ResourcesMgr.getInstance().engine.registerUpdateHandler(new TimerHandler(.5f, new ITimerCallback() {
                    @Override
                    public void onTimePassed(TimerHandler pTimerHandler) {
                        ResourcesMgr.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                        if (!mIsDestroyed) {
                            soundAlreadyPlaying = false;
                        }
                    }
                }));
            }

            lifePoints -= impulse.len();

            //Splash
            mScene.splashManager.createSplash(mEntity.getX(), mEntity.getY(), this.color);


            if (lifePoints<=0) {
                mScene.notifyEnemyKilled(this);
                destroy();
                return;
            }

            state = EnemyState.STUNNED;
            this.animate (Animation.STUNNED);

            ResourcesMgr.getInstance().engine.registerUpdateHandler(new TimerHandler(STUNNED_TIME, new ITimerCallback() {
                @Override
                public void onTimePassed(TimerHandler pTimerHandler) {

                    ResourcesMgr.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                    if (!mIsDestroyed) {
                        state = EnemyState.RECOVERING;
                        animate(Animation.WALKING);
                    }
                }
            }));
        }
    }



    public void animate(Animation state) {
        this.animate(state);
    }

    public void pause (float pauseDuration) {
        state = EnemyState.PAUSED;
        ((AnimatedSprite) mEntity).stopAnimation();
        ResourcesMgr.getInstance().engine.registerUpdateHandler(new TimerHandler(pauseDuration, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                ResourcesMgr.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                if (!mIsDestroyed) {
                    state = EnemyState.WALKING;
                    animate(Animation.WALKING);
                }
            }
        }));
    }

    public void callPhysicObjectUpdate (float pSecondsElapsed) {
        super.onUpdate(pSecondsElapsed);
    }

    @Override
    public void onBeginContact(Contact pContact) {
        if (state == EnemyState.SPAWNING){
            state = EnemyState.WALKING;
        }
        //Log.d(Constants.TAG, "Enemy beginContact " + pContact.getFixtureA().getBody().getUserData()+ " with " + pContact.getFixtureB().getBody().getUserData());
    }

    @Override
    public void onEndContact(Contact pContact) {

    }

    @Override
    public void onPreSolve(Contact pContact, Manifold pOldManifold) {

    }

    @Override
    public void onPostSolve(Contact pContact, ContactImpulse pImpulse)  {

    }

    @Override
    public void onUpdate (float pSecondsElapsed) {
        super.onUpdate(pSecondsElapsed);

        if (mScene.ringinClock != null && lured == false) { // Start ringing
            lured = true;
            pause(PAUSED_TIME * MathUtils.random(.8f, 1.2f));

        } else if (lured == true && mScene.ringinClock == null ) { // Stop ringing
            lured = false;
            pause(PAUSED_TIME * MathUtils.random(.8f, 1.2f));
            direction = -1;

        }

        if (lured) {
            // Walk to the clock
            direction = mScene.ringinClock.mBody.getPosition().x - mBody.getPosition().x;
            if (Math.abs(direction) < 1) { // Close to the clock
                pause(1f);
            } else {
                direction = Math.signum(direction);
            }
        }


        switch (state) {
            case PAUSED:
                mBody.setTransform(mBody.getPosition().x, mBody.getPosition().y, 0);
                lastSpeed2 = 0;
                break;

            case WALKING:
                // Face always walking direction
                ((Sprite) mEntity).setFlippedHorizontal(direction > 0);

                // Always standing up
                mBody.setTransform(mBody.getPosition().x, mBody.getPosition().y, 0);

                currentSpeed2 = mBody.getLinearVelocity().len2();

                if (currentSpeed2 < speed2) { // Too slow
                    mBody.applyLinearImpulse(new Vector2(.3f * direction * mBody.getMass(), 0), mBody.getWorldCenter());
                }
                else if (currentSpeed2 > speed2) { // Too fast
                    if(Math.abs(mBody.getLinearVelocity().x) > speed) {
                        mBody.setLinearVelocity(speed * direction, mBody.getLinearVelocity().y);
                    }
                }

                if (currentSpeed2 < (speed2 * .1f)  && currentSpeed2 < lastSpeed2   // Moving so slow and decelerating
                        ||  Math.signum(mBody.getLinearVelocity().x) != direction ){ // Or moving backwards
                    // jump!
                    mBody.setLinearVelocity(1.5f * direction * speed, jumpPower );
                }

                lastSpeed2 = currentSpeed2;
                break;

            case RECOVERING:
                float angle = mBody.getAngle() % (2 * (float) Math.PI);
                if (angle > .2f) {
                    mBody.setTransform(mBody.getPosition().x, mBody.getPosition().y, angle - .2f);
                } else if (angle < -.2f) {
                    mBody.setTransform(mBody.getPosition().x, mBody.getPosition().y, angle + .2f);
                } else {
                    mBody.setTransform(mBody.getPosition().x, mBody.getPosition().y, 0);

                    state = EnemyState.WALKING;
                }
                lastSpeed2 = 0;
                break;

        }
    }

}
