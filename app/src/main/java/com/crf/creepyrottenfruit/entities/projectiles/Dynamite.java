package com.crf.creepyrottenfruit.entities.projectiles;

import android.content.Context;
import android.os.Vibrator;
import android.util.Log;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.entities.enemies.Enemy;
import com.crf.creepyrottenfruit.entities.ObjectBit;
import com.crf.creepyrottenfruit.entities.PhysicalObject;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.particle.SpriteParticleSystem;
import org.andengine.entity.particle.emitter.PointParticleEmitter;
import org.andengine.entity.particle.initializer.AlphaParticleInitializer;
import org.andengine.entity.particle.initializer.RotationParticleInitializer;
import org.andengine.entity.particle.initializer.ScaleParticleInitializer;
import org.andengine.entity.particle.modifier.AlphaParticleModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.util.adt.color.Color;
import org.andengine.util.math.MathUtils;

import static org.andengine.extension.physics.box2d.util.constants.PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;

/*
 * Created by Jose Maria Olea on 10/11/2017.
 */

public class Dynamite extends Projectile{

    // ====================================================
    // CONSTANTS
    // ====================================================
    private static final float DENSITY = .5f;
    private static final float ELASTICITY = .0f;
    private static final float FRICTION = .5f;
    private static final Color COLOR = new Color (158f/255,11f/255,15f/255);

    private static final float DESTRUCTION_RADIUS = 200/PIXEL_TO_METER_RATIO_DEFAULT; //6.25
    private static final float IMPULSE_RADIUS = 400/PIXEL_TO_METER_RATIO_DEFAULT; //12.5
    private static final float EXPLOSION_POWER = 800;
    private static final float COUNTDOWN_TIME = 3f;


    // ====================================================
    // VARIABLES
    // ====================================================
    SpriteParticleSystem particleSystem;

    // ====================================================
    // CONSTRUCTOR
    // ====================================================
    public Dynamite(float x, float y, float powerX, float powerY, GameScene scene) {
        mPhysicsWorld = scene.mPhysicsWorld;
        mScene = scene;

        mEntity = new Sprite(x,y,ResourcesMgr.dynamiteTexture,ResourcesMgr.getInstance().vertexBufferObjectManager);
        mEntity.setCullingEnabled(true);


        mEntity.setScale(.75f);

        mPhysicsConnector =  createPhysics(mPhysicsWorld,  powerX,  powerY);

        // Put the projectile in our world
        mScene.getChildByIndex(Constants.LAYER_ITEMS).attachChild(mEntity);
        mPhysicsWorld.registerPhysicsConnector(mPhysicsConnector);

    }

    public PhysicsConnector createPhysics(PhysicsWorld physicsWorld, float powerX, float powerY) {

        final float angularVelocity = (float) (Math.PI * (Math.random() - .5d));
        

        float width = mEntity.getScaleX() * mEntity.getWidth()/PIXEL_TO_METER_RATIO_DEFAULT;
        float height = mEntity.getScaleY() *mEntity.getHeight()/PIXEL_TO_METER_RATIO_DEFAULT;

        Vector2[] vertex =  new Vector2 [] {new Vector2( -0.34810f*width, -0.45000f*height),
                new Vector2( +0.46203f*width, -0.43000f*height),
                new Vector2( +0.45570f*width, +0.36000f*height),
                new Vector2( -0.32278f*width, +0.41000f*height)};


        FixtureDef fixture = PhysicsFactory.createFixtureDef(DENSITY,ELASTICITY,FRICTION, false,mScene.CATEGORYBIT_PROJECTILES, mScene.MASKBITS_PROJECTILES, (short)0);
        mBody = PhysicsFactory.createPolygonBody(physicsWorld, mEntity, vertex, BodyDef.BodyType.DynamicBody, fixture);
        mBody.setAngularVelocity(angularVelocity);
        mBody.applyLinearImpulse(powerX * mBody.getMass(), powerY * mBody.getMass(), mBody.getWorldCenter().x, mBody.getWorldCenter().y);
        mBody.setUserData(this);

        return new PhysicsConnector(mEntity, mBody, true, true);
    }

    public void explode () {
        Log.d(Constants.TAG, "Boom!");

        ResourcesMgr.ticktackSound.stop();
        ResourcesMgr.explosionSound.setRate(MathUtils.random(.8f,1.2f));
        ResourcesMgr.explosionSound.play();

        Vibrator v = (Vibrator) ResourcesMgr.getInstance().activity.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        v.vibrate(200);

        mPhysicsWorld.QueryAABB(new QueryCallback() {
            @Override
            public boolean reportFixture(Fixture fixture) {

                if (fixture.getBody().getUserData() instanceof PhysicalObject) {
                    PhysicalObject target = (PhysicalObject) fixture.getBody().getUserData();

                    if (target != Dynamite.this) { // Dont destroy self object

                        if (target.mBody.getType()== BodyDef.BodyType.DynamicBody) { // Dont destroy ground and fixed objects

                            Log.d(Constants.TAG, "Find neighbor " + target);
                            Vector2 impulse = target.mBody.getPosition().sub(Dynamite.this.mBody.getPosition());
                            float distance = impulse.len();

                            if (distance < IMPULSE_RADIUS) {
                                float power = Math.max(0, EXPLOSION_POWER * (DESTRUCTION_RADIUS - distance));
                                impulse = impulse.nor().mul(power);
                                target.mBody.applyLinearImpulse(impulse.x, impulse.y, target.mBody.getPosition().x, target.mBody.getPosition().y);
                                if (target instanceof Enemy) {
                                    ((Enemy)target).hit(impulse.mul(10f));
                                }
                            }

                            if (distance < DESTRUCTION_RADIUS) {
                                if (target instanceof Enemy || target instanceof Box) {
                                    target.destroy();
                                }
                            }
                        }
                    }
                }

                return true;
            }
        }, mBody.getPosition().x - IMPULSE_RADIUS, mBody.getPosition().y - IMPULSE_RADIUS ,
                mBody.getPosition().x + IMPULSE_RADIUS, mBody.getPosition().y + IMPULSE_RADIUS);


        PointParticleEmitter particleEmitter = new PointParticleEmitter(mEntity.getX(), mEntity.getY());
        particleSystem = new SpriteParticleSystem(particleEmitter,1000,1000,1,ResourcesMgr.explosionTexture, ResourcesMgr.getInstance().vertexBufferObjectManager);
        particleSystem.addParticleInitializer(new AlphaParticleInitializer<Sprite>(1f));
        particleSystem.addParticleInitializer(new ScaleParticleInitializer<Sprite>(2.5f));
        particleSystem.addParticleInitializer(new RotationParticleInitializer<Sprite>((float) (Math.random() * 2 * Math.PI)) );
        particleSystem.addParticleModifier(new AlphaParticleModifier<Sprite>(0,.5f,1f,0f));
        mScene.getChildByIndex(Constants.LAYER_EFFECTS).attachChild(particleSystem);

        ResourcesMgr.getInstance().engine.registerUpdateHandler(new TimerHandler(1, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                ResourcesMgr.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                particleSystem.detachSelf(); // Remove particle system when it has finished
            }
        }));
        Log.d(Constants.TAG, "Destroy dinamite");
        this.destroy();
    }

    @Override
    public void destroy() {


        if(!mIsDestroyed)
            ResourcesMgr.getInstance().activity.runOnUpdateThread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < 8; i++) {
                        float scale = .2f + .3f*(float) Math.random(); // Between .2 and .5
                        ObjectBit bit = new ObjectBit(mEntity.getX() + (float) Math.random(), mEntity.getY() + (float) Math.random(), scale, mScene);
                        bit.mEntity.setColor(COLOR);
                        bit.mBody.setLinearVelocity(20 * MathUtils.random(-1f,1f), 20 * MathUtils.random(-1f,1f));
                    }
                    //mScene.unregisterTouchArea(mEntity);
                    mPhysicsWorld.unregisterPhysicsConnector(mPhysicsConnector);
                    mPhysicsWorld.destroyBody(mBody);
                    mEntity.detachSelf();
                    mEntity.dispose();
                    mEntity = null;
                    mBody = null;
                }});
        mIsDestroyed = true;
    }

    @Override
    public void onBeginContact(Contact pContact) {

        Fixture x1 = pContact.getFixtureA();
        Fixture x2 = pContact.getFixtureB();
        if ( ! (x1.getBody().getUserData() instanceof Dynamite)) {
            x2 = x1;
            x1 = pContact.getFixtureB();
        }
        assert (x1.getBody().getUserData() instanceof Dynamite);

        PhysicalObject projectile = (PhysicalObject) x1.getBody().getUserData();

        if (projectile.mBody.getLinearVelocity().len()>5 && !soundAlreadyPlaying) {
            //Log.d(Constants.TAG, "Box beginContact " + pContact.getFixtureA().getBody().getUserData()+ " with " + pContact.getFixtureB().getBody().getUserData() + " Speed: " + box.mBody.getLinearVelocity().len());
            ResourcesMgr.thumpSound.setRate(MathUtils.random(.8f,1.2f));
            ResourcesMgr.thumpSound.play();
            soundAlreadyPlaying = true;
            ResourcesMgr.getInstance().engine.registerUpdateHandler(new TimerHandler(.5f, new ITimerCallback() {
                @Override
                public void onTimePassed(TimerHandler pTimerHandler) {
                    ResourcesMgr.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                    if (!mIsDestroyed) {
                        soundAlreadyPlaying = false;
                    }
                }
            }));

        }

        // Set the  timer on collision
        ResourcesMgr.getInstance().engine.registerUpdateHandler(new TimerHandler(COUNTDOWN_TIME, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                ResourcesMgr.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                if (!mIsDestroyed) {
                    explode();
                }
            }
        }));

        ResourcesMgr.ticktackSound.setRate(MathUtils.random(.9f,1.1f));
        ResourcesMgr.ticktackSound.setPriority(5);
        ResourcesMgr.ticktackSound.play();

    }

    @Override
    public void onEndContact(Contact pContact) {

    }

    @Override
    public void onPreSolve(Contact pContact, Manifold pOldManifold) {

    }

    @Override
    public void onPostSolve(Contact pContact, ContactImpulse pImpulse)  {

    }
}
