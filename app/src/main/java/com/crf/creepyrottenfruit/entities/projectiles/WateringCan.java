package com.crf.creepyrottenfruit.entities.projectiles;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.entities.enemies.Enemy;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.util.math.MathUtils;

import static org.andengine.extension.physics.box2d.util.constants.PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;

/*
 * Created by Jose Maria Olea on 10/11/2017.
 */

public class WateringCan extends Projectile {

    // ====================================================
    // CONSTANTS
    // ====================================================
    private static final float DENSITY = .5f;
    private static final float ELASTICITY = .0f;
    private static final float FRICTION = .8f;

    // ====================================================
    // CONSTRUCTOR
    // ====================================================
    public WateringCan(float x, float y, float powerX, float powerY, GameScene scene) {
        mPhysicsWorld = scene.mPhysicsWorld;
        mScene = scene;

        float scale  = .75f;
        mEntity = new Sprite(x, y, ResourcesMgr.wateringCanTexture, ResourcesMgr.getInstance().vertexBufferObjectManager);
        mEntity.setCullingEnabled(true);
        mEntity.setScale(scale);

        mPhysicsConnector =  createPhysics(mPhysicsWorld,  powerX,  powerY);

        // Put the projectile in our world
        mScene.getChildByIndex(Constants.LAYER_BACKGROUND).attachChild(mEntity);
        mPhysicsWorld.registerPhysicsConnector(mPhysicsConnector);

        this.mEntity.registerUpdateHandler(this);

    }

    public PhysicsConnector createPhysics(PhysicsWorld physicsWorld, float powerX, float powerY) {

        final float angularVelocity = (float) (Math.PI * (Math.random() - .5d));



        float width = mEntity.getScaleX() * mEntity.getWidth()/PIXEL_TO_METER_RATIO_DEFAULT;
        float height = mEntity.getScaleY() *mEntity.getHeight()/PIXEL_TO_METER_RATIO_DEFAULT;

        Vector2[] vertex =  new Vector2 [] {new Vector2( -0.15094f*width, -0.47333f*height),
                new Vector2( +0.25943f*width, -0.46667f*height),
                new Vector2( +0.49057f*width, -0.09333f*height),
                new Vector2( +0.28302f*width, +0.28667f*height),
                new Vector2( -0.35849f*width, +0.30000f*height),
                new Vector2( -0.48113f*width, +0.06667f*height)};


        FixtureDef fixture = PhysicsFactory.createFixtureDef(DENSITY,ELASTICITY,FRICTION, false,mScene.CATEGORYBIT_PROJECTILES, mScene.MASKBITS_PROJECTILES, (short)0);
        mBody = PhysicsFactory.createPolygonBody(physicsWorld, mEntity, vertex, BodyDef.BodyType.DynamicBody, fixture);
        mBody.setAngularVelocity(angularVelocity);
        mBody.applyLinearImpulse(powerX * mBody.getMass(), powerY * mBody.getMass(), mBody.getWorldCenter().x, mBody.getWorldCenter().y);
        mBody.setUserData(this);

        return new PhysicsConnector(mEntity, mBody, true, true);
    }

    @Override
    public void onBeginContact(Contact pContact) {
        //if(pContact.isTouching()) {
        //Log.d(Constants.TAG, "WateringCan beginContact " + pContact.getFixtureA().getBody().getUserData()+ " with " + pContact.getFixtureB().getBody().getUserData());

        //}

        Fixture x1 = pContact.getFixtureA();
        Fixture x2 = pContact.getFixtureB();
        if ( ! (x1.getBody().getUserData() instanceof WateringCan)) {
            x2 = x1;
            x1 = pContact.getFixtureB();
        }
        assert (x1.getBody().getUserData() instanceof WateringCan);

        WateringCan can = (WateringCan) x1.getBody().getUserData();

        if (x2.getBody().getUserData() instanceof Enemy) {

            /* Box - Zombie collision */
            Enemy enemy = (Enemy) x2.getBody().getUserData();
            if (can.mBody.getLinearVelocity().len() > Constants.PROJECTILE_VELOCITY_TO_KILL) {

                // die monster die!
                enemy.hit(can.mBody.getLinearVelocity().nor().mul(BASE_DAMAGE));
            }
        }

        if (can.mBody.getLinearVelocity().len()>5 && !soundAlreadyPlaying) {
            ResourcesMgr.wateringCanCrashSound.setRate(MathUtils.random(.8f,1.2f));
            ResourcesMgr.wateringCanCrashSound.play();
            soundAlreadyPlaying = true;
            ResourcesMgr.getInstance().engine.registerUpdateHandler(new TimerHandler(.5f, new ITimerCallback() {
                @Override
                public void onTimePassed(TimerHandler pTimerHandler) {
                    ResourcesMgr.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                    if (!mIsDestroyed) {
                        soundAlreadyPlaying = false;
                    }
                }
            }));
        }
    }

    @Override
    public void onEndContact(Contact pContact) {

    }

    @Override
    public void onPreSolve(Contact pContact, Manifold pOldManifold) {

    }

    @Override
    public void onPostSolve(Contact pContact, ContactImpulse pImpulse)  {

    }
}
