package com.crf.creepyrottenfruit.entities.projectiles;

/*
 * Created by Jose Maria Olea on 03/01/2018.
 */

import com.crf.creepyrottenfruit.entities.PhysicalObject;

public abstract class Projectile extends PhysicalObject {

    protected final float BASE_DAMAGE = 150;
    protected boolean soundAlreadyPlaying = false;

    public Projectile () {
        super();
    }
}
