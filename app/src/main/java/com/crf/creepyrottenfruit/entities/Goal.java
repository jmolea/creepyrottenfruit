package com.crf.creepyrottenfruit.entities;

import android.util.Log;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.entities.enemies.Enemy;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.util.adt.color.Color;

/*
 * Created by Jose Maria Olea on 06/01/2018.
 */

public class Goal extends PhysicalObject {
    public Goal(float x, float y, float w, float h, GameScene scene) {
        mPhysicsWorld = scene.mPhysicsWorld;
        mScene = scene;


        mEntity = new Rectangle(x,y,w,h, ResourcesMgr.getInstance().vertexBufferObjectManager);
        mEntity.setColor(Color.GREEN);
        mEntity.setAlpha(.5f);
        mScene.getChildByIndex(Constants.LAYER_EFFECTS).attachChild(mEntity);
        if (!Constants.DEBUG_MODE) {
            mEntity.setVisible(false);
        }


        FixtureDef fixture = PhysicsFactory.createFixtureDef(0,0,0, true, mScene.CATEGORYBIT_GOAL, mScene.MASKBITS_GOAL, (short)0);
        mBody = PhysicsFactory.createBoxBody(mPhysicsWorld, x,y,w,h, BodyDef.BodyType.StaticBody, fixture);
        mBody.setUserData(this);

        mEntity.registerUpdateHandler(this);
    }

    @Override
    public void onBeginContact(Contact pContact) {
        Log.d(Constants.TAG, "Goal beginContact " + pContact.getFixtureA().getBody().getUserData()+ " with " + pContact.getFixtureB().getBody().getUserData());


        Fixture x1 = pContact.getFixtureA();
        Fixture x2 = pContact.getFixtureB();
        if ( ! (x1.getBody().getUserData() instanceof Goal)) {
            x2 = x1;
            x1 = pContact.getFixtureB();
        }
        assert (x1.getBody().getUserData() instanceof Goal);

        if (x2.getBody().getUserData() instanceof Enemy) {
            /* Zombie collision */

            // The enemy reached the goal. You lose a life.
            mScene.removeLife();
            Log.d(Constants.TAG, "Lives left: " + mScene.mLivesLeft);
        }
    }

    @Override
    public void onEndContact(Contact pContact) {

    }

    @Override
    public void onPreSolve(Contact pContact, Manifold pOldManifold) {

    }

    @Override
    public void onPostSolve(Contact pContact, ContactImpulse pImpulse)  {

    }
    // ====================================================
    // CONSTANTS
    // ====================================================

    // ====================================================
    // VARIABLES
    // ====================================================

    // ====================================================
    // CONSTRUCTOR
    // ====================================================

    // ====================================================
    // METHODS
    // ====================================================
}
