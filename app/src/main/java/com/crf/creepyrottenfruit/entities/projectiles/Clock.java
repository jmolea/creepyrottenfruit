package com.crf.creepyrottenfruit.entities.projectiles;

import android.content.Context;
import android.os.Vibrator;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.entities.PhysicalObject;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.util.adt.color.Color;
import org.andengine.util.math.MathUtils;

import static org.andengine.extension.physics.box2d.util.constants.PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;

/*
 * Created by Jose Maria Olea on 10/11/2017.
 */

public class Clock extends Projectile{

    // ====================================================
    // ENUMS
    // ====================================================

    private enum State {
        CREATED,
        COUNTDOWN,
        RINGING,
        FINISHED
    }

    // ====================================================
    // CONSTANTS
    // ====================================================
    private static final float DENSITY = .5f;
    private static final float ELASTICITY = .0f;
    private static final float FRICTION = .5f;
    private static final Color COLOR = new Color (158f/255,11f/255,15f/255);

    private static final float DESTRUCTION_RADIUS = 200/PIXEL_TO_METER_RATIO_DEFAULT; //6.25
    private static final float IMPULSE_RADIUS = 400/PIXEL_TO_METER_RATIO_DEFAULT; //12.5
    private static final float EXPLOSION_POWER = 800;
    private static final float COUNTDOWN_TIME = 3f;
    private static final float ALARM_DURATION = 8f;

    //final float SIZE = 80;

    // ====================================================
    // VARIABLES
    // ====================================================
    private State state = State.CREATED;

    // ====================================================
    // CONSTRUCTOR
    // ====================================================
    public Clock(float x, float y, float powerX, float powerY, GameScene scene) {

        mPhysicsWorld = scene.mPhysicsWorld;
        mScene = scene;

        mEntity = new Sprite(x,y,ResourcesMgr.clockTexture,ResourcesMgr.getInstance().vertexBufferObjectManager);
        mEntity.setCullingEnabled(true);

        mEntity.setScale(.75f);

        mPhysicsConnector =  createPhysics(mPhysicsWorld,  powerX,  powerY);

        // Put the projectile in our world
        mScene.getChildByIndex(Constants.LAYER_ITEMS).attachChild(mEntity);
        mPhysicsWorld.registerPhysicsConnector(mPhysicsConnector);

        mScene.registerTouchArea(mEntity);

        this.mEntity.registerUpdateHandler(this);

    }

    public PhysicsConnector createPhysics(PhysicsWorld physicsWorld, float powerX, float powerY) {

        final float angularVelocity = (float) (Math.PI * (Math.random() - .5d));
        

        float width = mEntity.getScaleX() * mEntity.getWidth()/PIXEL_TO_METER_RATIO_DEFAULT;
        float height = mEntity.getScaleY() *mEntity.getHeight()/PIXEL_TO_METER_RATIO_DEFAULT;

        Vector2[] vertex =  new Vector2 [] {new Vector2( -0.44681f*width, -0.45333f*height),
                new Vector2( +0.39716f*width, -0.45333f*height),
                new Vector2( +0.49645f*width, +0.33333f*height),
                new Vector2( +0.25532f*width, +0.50000f*height),
                new Vector2( -0.30496f*width, +0.52000f*height),
                new Vector2( -0.50355f*width, +0.37333f*height),
        };


        FixtureDef fixture = PhysicsFactory.createFixtureDef(DENSITY,ELASTICITY,FRICTION, false,mScene.CATEGORYBIT_PROJECTILES, mScene.MASKBITS_PROJECTILES, (short)0);
        mBody = PhysicsFactory.createPolygonBody(physicsWorld, mEntity, vertex, BodyDef.BodyType.DynamicBody, fixture);
        mBody.setAngularVelocity(angularVelocity);
        mBody.applyLinearImpulse(powerX * mBody.getMass(), powerY * mBody.getMass(), mBody.getWorldCenter().x, mBody.getWorldCenter().y);
        mBody.setUserData(this);

        return new PhysicsConnector(mEntity, mBody, true, true);
    }

    public void startAlarm () {
        state = State.RINGING;
        Vibrator v = (Vibrator) ResourcesMgr.getInstance().activity.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(300);

        ResourcesMgr.alarmSound.setRate(MathUtils.random(.9f,1.1f));
        ResourcesMgr.alarmSound.setPriority(5);
        ResourcesMgr.alarmSound.play();
        mScene.ringinClock = this;

        ResourcesMgr.getInstance().engine.registerUpdateHandler(new TimerHandler(ALARM_DURATION, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                ResourcesMgr.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                if (!mIsDestroyed) {
                    stopAlarm();
                }
            }
        }));

    }

    public void stopAlarm () {
        state = State.FINISHED;
        mScene.ringinClock = null;
    }



    @Override
    public void onBeginContact(Contact pContact) {

        Fixture x1 = pContact.getFixtureA();
        Fixture x2 = pContact.getFixtureB();
        if ( ! (x1.getBody().getUserData() instanceof Clock)) {
            x2 = x1;
            x1 = pContact.getFixtureB();
        }
        assert (x1.getBody().getUserData() instanceof Clock);

        PhysicalObject projectile = (PhysicalObject) x1.getBody().getUserData();

        if (projectile.mBody.getLinearVelocity().len()>5 && !soundAlreadyPlaying && state != State.RINGING) {
            ResourcesMgr.clockHitSound.setRate(MathUtils.random(.8f,1.2f));
            ResourcesMgr.clockHitSound.play();
            soundAlreadyPlaying = true;
            ResourcesMgr.getInstance().engine.registerUpdateHandler(new TimerHandler(.5f, new ITimerCallback() {
                @Override
                public void onTimePassed(TimerHandler pTimerHandler) {
                    ResourcesMgr.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                    if (!mIsDestroyed) {
                        soundAlreadyPlaying = false;
                    }
                }
            }));

        }

        // Set the alarm timer on collision
        if (state == State.CREATED) {
            state = State.COUNTDOWN;
            ResourcesMgr.getInstance().engine.registerUpdateHandler(new TimerHandler(COUNTDOWN_TIME, new ITimerCallback() {
                @Override
                public void onTimePassed(TimerHandler pTimerHandler) {
                    ResourcesMgr.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                    if (!mIsDestroyed) {

                        startAlarm();
                    }
                }
            }));
        }

    }

    @Override
    public void onEndContact(Contact pContact) {

    }

    @Override
    public void onPreSolve(Contact pContact, Manifold pOldManifold) {

    }

    @Override
    public void onPostSolve(Contact pContact, ContactImpulse pImpulse)  {

    }

    @Override
    public void onUpdate(float pSecondsElapsed) {
        super.onUpdate(pSecondsElapsed);

        if (!mIsDestroyed) {
            if (state == State.RINGING) {
                if (Math.random()<.25) {
                    mBody.setLinearVelocity( MathUtils.random(-3f,3f), MathUtils.random(-3f,3f));
                    mBody.setAngularVelocity(MathUtils.random(-1f,1f));
                }
            }
        }

    }
}
