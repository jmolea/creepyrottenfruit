package com.crf.creepyrottenfruit.entities;

import android.util.Log;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.shape.IShape;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsWorld;

/*
 * Created by Jose Maria Olea on 29/12/2017.
 */

public abstract class PhysicalObject<T extends IShape> implements ContactListener, IUpdateHandler {

    // ====================================================
    // VARIABLES
    // ====================================================

    public Body mBody;
    public T mEntity;
    public GameScene mScene;
    public PhysicsWorld mPhysicsWorld;
    public PhysicsConnector mPhysicsConnector;
    public boolean mIsDestroyed = false;

    private boolean mBeginContactAlreadyCalled = false;
    private boolean mEndContactAlreadyCalled = false;
    private boolean mPostSolveReadyToBeCalled = false;
    private boolean mIsBurning = false;

    float mMaxImpulse;

    public boolean mHasExpiration;
    public float fadeOutLength;
    public float timeLeft;

    // ====================================================
    // ABSTRACT METHODS
    // ====================================================
    public abstract void onBeginContact(Contact pContact);
    public abstract void onEndContact(Contact pContact);
    public abstract void onPreSolve(Contact pContact, Manifold pOldManifold);
    public abstract void onPostSolve(Contact pContact, ContactImpulse pImpulse);

    // ====================================================
    // METHODS
    // ====================================================

    public void burn () {
        if (!mIsBurning) {
            mIsBurning = true;
        }
    }

    public void destroy() {

        if (!mIsDestroyed) {
            assert (mBody!=null);

            ResourcesMgr.getInstance().activity.runOnUpdateThread(new Runnable() {
                @Override
                public void run() {
                    if (mBody == null) {
                        Log.d(Constants.TAG, "WTF?");
                    }
                    //Log.d(Constants.TAG, "Destroying now " + this);
                    mScene.unregisterUpdateHandler(PhysicalObject.this);
                    mPhysicsWorld.unregisterPhysicsConnector(mPhysicsConnector);
                    mPhysicsWorld.destroyBody(mBody);
                    mEntity.clearUpdateHandlers();
                    mEntity.clearEntityModifiers();
                    mEntity.detachSelf();
                    mEntity.dispose();
                    mEntity = null;
                    mBody = null;
                }
            });

            mIsDestroyed = true;
        }
    }

    @Override
    public void onUpdate(float pSecondsElapsed) {
        this.mBeginContactAlreadyCalled = false;
        this.mEndContactAlreadyCalled = false;

    }
    @Override public void reset() {}

    @Override
    public void beginContact(Contact contact) {
        if(!this.mBeginContactAlreadyCalled) {
            this.mBeginContactAlreadyCalled = true;
            this.onBeginContact(contact);
        }
    }

    @Override
    public void endContact(Contact contact) {
        if(!this.mEndContactAlreadyCalled) {
            this.mEndContactAlreadyCalled = true;
            this.onEndContact(contact);
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        this.onPreSolve(contact, oldManifold);
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
        //this.mPostSolveReadyToBeCalled = true;
        /*Log.d(Constants.TAG, "Normal impulse: " + impulse.getNormalImpulses()[0] + ", " +impulse.getNormalImpulses()[1]);
        float CurrentMaxImpulse = impulse.getNormalImpulses()[0];
        for(int i = 1; i < impulse.getNormalImpulses().length; i++)
            CurrentMaxImpulse = Math.max(impulse.getNormalImpulses()[i], CurrentMaxImpulse);
        CurrentMaxImpulse /= this.mBody.getMass();

        mMaxImpulse += CurrentMaxImpulse;*/

        this.onPostSolve(contact, impulse);
    }

}
