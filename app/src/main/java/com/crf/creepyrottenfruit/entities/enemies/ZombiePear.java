package com.crf.creepyrottenfruit.entities.enemies;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.entities.FruitBit;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.util.adt.color.Color;
import org.andengine.util.math.MathUtils;

import static org.andengine.extension.physics.box2d.util.constants.PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;

/*
 * Created by Jose Maria Olea on 07/01/2018.
 */

public class ZombiePear extends Enemy implements IUpdateHandler {
    // ====================================================
    // CONSTANTS
    // ====================================================
    private final float JUMP_POWER = 8f;
    private final float SPEED = 1f;
    private final float LIFE_POINTS =220;
    private static final Color COLOR = new Color (228f/255,182f/255,154f/255);

    private static final Color COLOR_OUT =  new Color (78f/255,126f/255,71f/255);
    private static final float BIT_SCALE_FACTOR = .6f;


    // ====================================================
    // VARIABLES
    // ====================================================

    // ====================================================
    // CONSTRUCTOR
    // ====================================================

    public ZombiePear(float x, float y, GameScene scene) {
        mPhysicsWorld = scene.mPhysicsWorld;
        mScene = scene;

        mEntity = new AnimatedSprite(x, y + 50, ResourcesMgr.zombiePearTextureRegion, ResourcesMgr.getInstance().vertexBufferObjectManager) ;
        mEntity.setCullingEnabled(true);
        ((AnimatedSprite)mEntity).animate(new long[]{90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90}, 0, 15, true);

        mPhysicsConnector = createPhysics(mPhysicsWorld);

        // Put the zombie in our world
        mScene.getChildByIndex(Constants.LAYER_CHARACTERS).attachChild(mEntity);
        mPhysicsWorld.registerPhysicsConnector(mPhysicsConnector);

        //set();

        this.mEntity.registerUpdateHandler(this);

        lifePoints = this.LIFE_POINTS;
        speed = this.SPEED;
        speed2 = SPEED * SPEED;
        jumpPower = this.JUMP_POWER;
        color = this.COLOR;

    }
    // ====================================================
    // METHODS
    // ====================================================

    private PhysicsConnector createPhysics (PhysicsWorld physicsWorld) {

        FixtureDef fixture = PhysicsFactory.createFixtureDef(DENSITY,ELASTICITY,FRICTION, false, mScene.CATEGORYBIT_ENEMIES, mScene.MASKBITS_ENEMIES, (short)0);
        float width = mEntity.getWidth()/PIXEL_TO_METER_RATIO_DEFAULT;
        float height = mEntity.getHeight()/PIXEL_TO_METER_RATIO_DEFAULT;

        Vector2[] vertex = new Vector2[]{new Vector2( -0.34211f*width, -0.13450f*height),
                new Vector2( -0.00877f*width, -0.47368f*height),
                new Vector2( +0.11404f*width, -0.47368f*height),
                new Vector2( +0.51754f*width, -0.15205f*height),
                new Vector2( +0.16667f*width, +0.36257f*height),
                new Vector2( -0.11404f*width, +0.35088f*height)
        };


        mBody = PhysicsFactory.createPolygonBody(physicsWorld, mEntity, vertex, BodyDef.BodyType.DynamicBody, fixture);
        mBody.setUserData(this);

        return new PhysicsConnector(mEntity, mBody, true, true);
    }

    @Override
    public void animate (Animation animation) {
        if (mEntity != null) {
            switch (animation) {
                case WALKING:
                    ((AnimatedSprite) mEntity).animate(new long[]{90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90}, 0, 15, true);
                    break;
                case STUNNED:
                    ((AnimatedSprite) mEntity).stopAnimation(16);
                    break;
                case SPAWNING:
                    ((AnimatedSprite) mEntity).stopAnimation(1);
            }
        }
    }

    @Override
    public void destroy () {

            if (!mIsDestroyed) {

                //Splash
                mScene.splashManager.createSplash(mEntity.getX(), mEntity.getY(), this.color);


            ResourcesMgr.getInstance().engine.runOnUpdateThread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < AMOUNT_OF_PIECES_WHEN_DESTROYED; i++) {

                        FruitBit bit = new FruitBit(mEntity.getX() + (float) Math.random(), mEntity.getY() + (float) Math.random(), BIT_SCALE_FACTOR, COLOR, COLOR_OUT, mScene);
                        //if (impulse != null) {
                        Vector2 randomImpulse = new Vector2(10 * MathUtils.random(-1f,1f), 10 * MathUtils.random(-1f,1f));
                        bit.mBody.setLinearVelocity(mBody.getLinearVelocity().add(randomImpulse) );
                        //}
                    }

                    mScene.unregisterUpdateHandler(ZombiePear.this);
                    mPhysicsWorld.unregisterPhysicsConnector(mPhysicsConnector);
                    mPhysicsWorld.destroyBody(mBody);
                    mEntity.clearUpdateHandlers();
                    mEntity.clearEntityModifiers();
                    mEntity.detachSelf();
                    mEntity.dispose();
                    mEntity = null;
                    mBody = null;

                }
            });

            mIsDestroyed = true;
            mScene.enemiesLeft --;
        }

    }


    @Override
    public void reset() {

    }
}
