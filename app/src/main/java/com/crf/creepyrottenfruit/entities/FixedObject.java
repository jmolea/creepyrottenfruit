package com.crf.creepyrottenfruit.entities;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Manifold;

/**
 * Created by Jose Maria Olea on 14/01/2018.
 */

public class FixedObject extends PhysicalObject {
    @Override
    public void onBeginContact(Contact pContact) {

    }

    @Override
    public void onEndContact(Contact pContact) {

    }

    @Override
    public void onPreSolve(Contact pContact, Manifold pOldManifold) {

    }

    @Override
    public void onPostSolve(Contact pContact, ContactImpulse pImpulse) {

    }
}
