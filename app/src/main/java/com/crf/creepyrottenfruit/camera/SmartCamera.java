package com.crf.creepyrottenfruit.camera;

import com.crf.creepyrottenfruit.Constants;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.camera.ZoomCamera;

/*
 * Created by Jose Maria Olea on 29/12/2017.
 */

public class SmartCamera extends SmoothCamera {

    private float mMaxZoomFactor;
    private float mMinZoomFactor;
    boolean mLimitZoomEnabled = false;

    public SmartCamera(final float pX, final float pY, final float pWidth, final float pHeight) {
        super(pX, pY, pWidth, pHeight,1000,1000,1000);
    }

    @Override
    public void setZoomFactor(float pZoomFactor) {
        if (mLimitZoomEnabled) {
            if (pZoomFactor > mMaxZoomFactor) {
                pZoomFactor = mMaxZoomFactor;
            } else if (pZoomFactor < mMinZoomFactor) {
                pZoomFactor = mMinZoomFactor;
            }
        }

        super.setZoomFactor(pZoomFactor);
    }

    public void setMaxZoomFactor(float maxZoomFactor) {
        this.mMaxZoomFactor = maxZoomFactor;
    }

    public void setMinZoomFactor(float minZoomFactor) {
        this.mMinZoomFactor = minZoomFactor;
    }

    public void setLimitZoomEnabled(boolean pLimitZoomEnabled) {
        this.mLimitZoomEnabled = pLimitZoomEnabled;
    }

    public float getMaxZoomFactor() {
        return mMaxZoomFactor;
    }

    public float getMinZoomFactor() {
        return mMinZoomFactor;
    }

    public boolean isLimitZoomEnabled() {
        return mLimitZoomEnabled;
    }

    @Override
    public void reset() {
        set(0, 0, Constants.CAMERA_WIDTH, Constants.CAMERA_HEIGHT);
        setCenter (this.getCenterX(), this.getCenterY());
        setZoomFactorDirect(1.0f);
        setChaseEntity(null);
        setHUD(null);

    }
}
