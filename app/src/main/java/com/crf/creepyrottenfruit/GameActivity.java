package com.crf.creepyrottenfruit;

import android.util.DisplayMetrics;
import android.view.Display;

import com.crf.creepyrottenfruit.camera.SmartCamera;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.scenes.SceneMgr;

import org.andengine.engine.Engine;
import org.andengine.engine.LimitedFPSEngine;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.BaseGameActivity;

import java.io.IOException;

/*
 * Created by Jose Maria Olea on 29/12/2017.
 */

public class GameActivity extends BaseGameActivity {

    private SmartCamera camera;
    ResourcesMgr resourcesMgr;

    @Override
    public EngineOptions onCreateEngineOptions() {

        camera = new SmartCamera(0, 0, Constants.CAMERA_WIDTH, Constants.CAMERA_HEIGHT);
        EngineOptions options = new EngineOptions(true, ScreenOrientation.LANDSCAPE_SENSOR, new FillResolutionPolicy(), camera);
        options.getAudioOptions().setNeedsSound(true);
        options.getAudioOptions().setNeedsMusic(true);
        options.getAudioOptions().getSoundOptions().setMaxSimultaneousStreams(3);

        return options;

    }

    @Override
    public Engine onCreateEngine (EngineOptions engineOptions) {
        return new LimitedFPSEngine(engineOptions, 60);
    }

    @Override
    public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws IOException {

        ResourcesMgr.initialize(this.getEngine(), this, camera, getVertexBufferObjectManager());
        resourcesMgr = ResourcesMgr.getInstance();
        pOnCreateResourcesCallback.onCreateResourcesFinished();

    }

    @Override
    public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws IOException{

        SceneMgr.getInstance().createSplashScene(pOnCreateSceneCallback);

    }

    @Override
    public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws IOException {
        mEngine.registerUpdateHandler(new TimerHandler(2f, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                mEngine.unregisterUpdateHandler(pTimerHandler);
                SceneMgr.getInstance().disposeSplashScene();
                SceneMgr.getInstance().createMenuScene();
            }
        }));
        pOnPopulateSceneCallback.onPopulateSceneFinished();
    }

    @Override
    protected void onPause()
    {

        if (this.isGameLoaded() && SceneMgr.getInstance().gameScene != null) {
                // If in game, back to the menu
                SceneMgr.getInstance().getCurrentScene().pause();
                SceneMgr.getInstance().createMenuScene();
        }
        super.onPause();

    }

    @Override
    protected synchronized void onResume()
    {
        super.onResume();
        System.gc();

    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if (this.isGameLoaded())
        {
            System.exit(0);
        }
    }

    @Override
    public void onBackPressed()
    {
        if (this.isGameLoaded())
        {
            if (SceneMgr.getInstance().getCurrenteSceneType() == SceneMgr.SceneType.SCENE_GAME) {
                SceneMgr.getInstance().getCurrentScene().pause();
                SceneMgr.getInstance().createMenuScene();
            }

            else {
                if (SceneMgr.getInstance().gameScene != null) {
                    SceneMgr.getInstance().gameScene.disposeScene();
                }
                SceneMgr.getInstance().getCurrentScene().disposeScene();
                System.exit(0);
            }

        }
    }


}