package com.crf.creepyrottenfruit;

import android.app.Activity;

import com.crf.creepyrottenfruit.core.Parallax2DBackground;
import com.crf.creepyrottenfruit.core.ResourcesMgr;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.scene.background.Background;
import org.andengine.opengl.util.GLState;
import org.andengine.ui.activity.BaseGameActivity;

/*
 * Created by Jose Maria Olea on 29/12/2017.
 */

public class BackgroundFactory {

    public static Background createMarketBackground () {

        final Parallax2DBackground parallaxBackground = new Parallax2DBackground(0, 0, 0);

        Sprite parallaxLayerSkySprite = new Sprite(ResourcesMgr.skyTexture.getWidth()/2, ResourcesMgr.skyTexture.getHeight()/2, ResourcesMgr.skyTexture,  ResourcesMgr.getInstance().vertexBufferObjectManager) {
            @Override
            protected void preDraw(final GLState pGLState, final Camera pCamera)
            {
                super.preDraw(pGLState, pCamera);
                pGLState.enableDither();
            }

            @Override protected void postDraw(GLState pGLState, Camera pCamera)
            {
                // Disable dithering
                pGLState.disableDither();
                super.postDraw(pGLState, pCamera);
            }
        };

        Parallax2DBackground.Parallax2DEntity p2De = new Parallax2DBackground.Parallax2DEntity(0.0f, 0f,0,0, parallaxLayerSkySprite);
        p2De.setFixed(true);
        parallaxBackground.attachParallax2DEntity(p2De);

        Sprite parallaxLayerCloudsSprite = new Sprite(ResourcesMgr.cloudsTexture.getWidth()/2, ResourcesMgr.cloudsTexture.getHeight()/2, ResourcesMgr.cloudsTexture,  ResourcesMgr.getInstance().vertexBufferObjectManager) ;
        p2De = new Parallax2DBackground.Parallax2DEntity(.1f, .1f,0,300,parallaxLayerCloudsSprite);
        p2De.setRepeatHorizontal(true);
        p2De.setmAnimate(true);
        p2De.setmHorizontalSpeed(-.4f);
        parallaxBackground.attachParallax2DEntity(p2De);


        Sprite parallaxLayerBackSprite = new Sprite(ResourcesMgr.marketBgTexture.getWidth()/2, ResourcesMgr.marketBgTexture.getHeight()/2, ResourcesMgr.marketBgTexture,  ResourcesMgr.getInstance().vertexBufferObjectManager) ;
        parallaxBackground.attachParallax2DEntity(new Parallax2DBackground.Parallax2DEntity(.55f , .70f,0,400,parallaxLayerBackSprite));

        Sprite parallaxLayerFrontSprite = new Sprite(ResourcesMgr.marketFgTexture.getWidth()/2, ResourcesMgr.marketFgTexture.getHeight()/2, ResourcesMgr.marketFgTexture,  ResourcesMgr.getInstance().vertexBufferObjectManager);
        parallaxBackground.attachParallax2DEntity(new Parallax2DBackground.Parallax2DEntity(1f, 1f,0,280, parallaxLayerFrontSprite));

        return parallaxBackground;
    }
}
