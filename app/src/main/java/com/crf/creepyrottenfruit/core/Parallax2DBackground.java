package com.crf.creepyrottenfruit.core;


import org.andengine.engine.camera.Camera;
import org.andengine.engine.camera.ZoomCamera;
import org.andengine.entity.IEntity;
import org.andengine.entity.scene.background.Background;
import org.andengine.opengl.util.GLState;
import org.andengine.util.debug.Debug;

import java.util.ArrayList;


/*
 * Created by Jose Maria Olea on 29/12/2017.
 */

public class Parallax2DBackground extends Background {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private final ArrayList<Parallax2DEntity> mParallaxEntities = new ArrayList<Parallax2DEntity>();
    private int mParallaxEntityCount;

    protected float mParallaxHorizontalValue, mParallaxVerticalValue;

    // ===========================================================
    // Constructors
    // ===========================================================

    public Parallax2DBackground(final float pRed, final float pGreen, final float pBlue) {
        super(pRed, pGreen, pBlue);
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public void setParallaxHorizontalValue(final float pParallaxHorizontalValue) {
        this.mParallaxHorizontalValue = pParallaxHorizontalValue;
    }

    public void setParallaxVerticalValue(final float pParallaxVerticalValue) {
        this.mParallaxVerticalValue = pParallaxVerticalValue;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void onDraw(final GLState pGLState, final Camera pCamera) {
        super.onDraw(pGLState, pCamera);

        final float parallaxHorizontalValue = this.mParallaxHorizontalValue;
        final float parallaxVerticalValue = this.mParallaxVerticalValue;
        final ArrayList<Parallax2DEntity> parallaxEntities = this.mParallaxEntities;

        for (int i = 0; i < this.mParallaxEntityCount; i++) {
            parallaxEntities.get(i).onDraw(pGLState, pCamera, parallaxHorizontalValue, parallaxVerticalValue);
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public void attachParallax2DEntity(final Parallax2DEntity pParallaxEntity) {
        this.mParallaxEntities.add(pParallaxEntity);
        this.mParallaxEntityCount++;
    }

    public boolean detachParallax2DEntity(final Parallax2DEntity pParallaxEntity) {
        this.mParallaxEntityCount--;
        final boolean success = this.mParallaxEntities.remove(pParallaxEntity);
        if (!success) {
            this.mParallaxEntityCount++;
        }
        return success;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    public static class Parallax2DEntity {
        // ===========================================================
        // Constants
        // ===========================================================

        // ===========================================================
        // Fields
        // ===========================================================

        private final float mParallaxHorizontalFactor;
        private final float mParallaxVerticalFactor;
        private float mParallaxHorizontalOffset;
        private float mParallaxVerticalOffset;
        private boolean mFixed;
        private boolean mRepeatHorizontal;
        private float mHorizontalSpeed;
        private float mVerticalSpeed;
        private boolean mAnimate;
        private final IEntity mEntity;

        // ===========================================================
        // Constructors
        // ===========================================================

        public Parallax2DEntity(final float pParallaxHorizontalFactor, final float pParallaxVerticalFactor, final float pParallaxHorizontalOffset,
                                final float pParallaxVerticalOffset, final IEntity pEntity) {
            this.mParallaxHorizontalFactor = pParallaxHorizontalFactor;
            this.mParallaxVerticalFactor = pParallaxVerticalFactor;
            this.mParallaxHorizontalOffset = pParallaxHorizontalOffset;
            this.mParallaxVerticalOffset = pParallaxVerticalOffset;
            this.mEntity = pEntity;
            this.mFixed = false;
            this.mRepeatHorizontal = false;
            this.mAnimate = false;
            this.mHorizontalSpeed = 0f;
            this.mVerticalSpeed = 0f;

            // TODO Adjust onDraw calculations, so that these assumptions aren't necessary.
            if (this.mEntity.getX() != 0) {
                Debug.w("The X position of a " + this.getClass().getSimpleName() + " is expected to be 0.");
            }

            if (this.mEntity.getOffsetCenterX() != 0) {
                Debug.w("The OffsetCenterXposition of a " + this.getClass().getSimpleName() + " is expected to be 0.");
            }

            if (this.mEntity.getY() != 0) {
                Debug.w("The Y position of a " + this.getClass().getSimpleName() + " is expected to be 0.");
            }

            if (this.mEntity.getOffsetCenterY() != 0) {
                Debug.w("The OffsetCenterY position of a " + this.getClass().getSimpleName() + " is expected to be 0.");
            }
        }

        // ===========================================================
        // Getter & Setter
        // ===========================================================

        public void setFixed(boolean mFixed) {
            this.mFixed = mFixed;
        }

        public void setRepeatHorizontal(boolean mRepeatHorizontal) {
            this.mRepeatHorizontal = mRepeatHorizontal;
        }

        public void setmHorizontalSpeed(float mHorizontalSpeed) {
            this.mHorizontalSpeed = mHorizontalSpeed;
        }

        public void setmVerticalSpeed(float mVerticalSpeed) {
            this.mVerticalSpeed = mVerticalSpeed;
        }

        public void setmAnimate(boolean mAnimate) {
            this.mAnimate = mAnimate;
        }

        // ===========================================================
        // Methods for/from SuperClass/Interfaces
        // ===========================================================

        // ===========================================================
        // Methods
        // ===========================================================

        public void onDraw(final GLState pGLState, final Camera pCamera,  float pParallaxHorizontalValue,  float pParallaxVerticalValue) {
            pGLState.pushModelViewGLMatrix();
            {

                float zoomFactor = 1.f;
                if (pCamera instanceof ZoomCamera) {
                    zoomFactor = ((ZoomCamera) pCamera ).getZoomFactor();
                }


                if (!mFixed){
                    float scaleX =  zoomFactor;
                    float scaleY = zoomFactor;

                    float offsetX = mParallaxHorizontalOffset + (pCamera.getWidth() - mEntity.getWidth()) / 2  - (mParallaxHorizontalFactor * pCamera.getCenterX());
                    float offsetY = mParallaxVerticalOffset + (pCamera.getHeight() - mEntity.getHeight()) / 2 - (mParallaxVerticalFactor * pCamera.getCenterY());



                    if (!mRepeatHorizontal) {
                        pGLState.scaleModelViewGLMatrixf(scaleX, scaleY, 0);
                        pGLState.translateModelViewGLMatrixf(offsetX, offsetY, 0);
                        this.mEntity.onDraw(pGLState, pCamera);
                    }
                    else if (mRepeatHorizontal){
                        pGLState.scaleModelViewGLMatrixf(scaleX, scaleY, 0);

                        float base = offsetX % mEntity.getWidth();
                        base -= mEntity.getWidth();
                        pGLState.translateModelViewGLMatrixf(base, offsetY, 0);
                        this.mEntity.onDraw(pGLState, pCamera);
                        int count = 0;
                        while (base + mEntity.getWidth() * (count) < pCamera.getWidth())
                        {
                            pGLState.translateModelViewGLMatrixf(mEntity.getWidth(), 0, 0);
                            this.mEntity.onDraw(pGLState, pCamera);
                            count++;
                        }
                        //Log.d(Constants.TAG,"Printed " + count + " apples");
                    }
                }

                else if (mFixed) {
                    this.mEntity.onDraw(pGLState, pCamera);
                }

                if (mAnimate) {
                    if (mRepeatHorizontal) {
                        mParallaxHorizontalOffset += mHorizontalSpeed;
                        mParallaxHorizontalOffset = mParallaxHorizontalOffset % mEntity.getWidth();
                    }
                }
            }
            pGLState.popModelViewGLMatrix();
        }

        // ===========================================================
        // Inner and Anonymous Classes
        // ===========================================================
    }
}
