package com.crf.creepyrottenfruit.core;

import android.graphics.Color;
import android.graphics.Typeface;

import com.crf.creepyrottenfruit.GameActivity;
import com.crf.creepyrottenfruit.GameOptions;
import com.crf.creepyrottenfruit.camera.SmartCamera;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder;
import org.andengine.opengl.texture.bitmap.AssetBitmapTexture;
import org.andengine.opengl.texture.bitmap.BitmapTextureFormat;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.debug.Debug;

import java.io.IOException;

/*
 * Created by Jose Maria Olea on 29/12/2017.
 */

public class ResourcesMgr {

    private static final ResourcesMgr INSTANCE = new ResourcesMgr();

    public Engine engine;
    public GameActivity activity;
    public SmartCamera camera;
    public VertexBufferObjectManager vertexBufferObjectManager;
    public GameOptions gameOptions;

    /* Textures */
    public static ITextureRegion splashBgTexture, splashTitleTexture, optionNewTexture, optionContinueTexture, hintScreenTexture;
    public static TiledTextureRegion musicIconTexture;

    public static ITextureRegion marketBgTexture, marketFgTexture, skyTexture, cloudsTexture, groundTexture;
    public static ITextureRegion boxSmallTexture, boxLargeTexture, splatTexture;
    public static ITextureRegion bottleTexture, objectBitTexture, ballTexture, wateringCanTexture, potTexture, dynamiteTexture, explosionTexture, clockTexture, flyTexture;
    public static ITextureRegion fruitBitBaseTexture [] , fruitBitTopTexture [];
    public static TiledTextureRegion cuttleryTexture;
    public static TiledTextureRegion itemsTexture;
    public static ITextureRegion shotButtonTexture, dotTexture, appleIconTexture, appleRottenIconTexture, powerSliderFrameTexture, powerSliderBgTexture;
    public static ITextureRegion onagreBody, onagreWheel, onagreArm, onagreTire;
    public static TiledTextureRegion zombieAppleTextureRegion, zombiePearTextureRegion, zombieBananaTextureRegion, zombieWatermelonTextureRegion, zombieGrapeTextureRegion;

    /* Fonts */
    public static Font defaultSmallFont;
    public static Font grinchedLargeFont, grinchedMediumFont, grinchedSmallFont;

    /* Sounds */
    public static Sound boxCrashSound, wateringCanCrashSound, bottleCrashSound, potCrashSound, explosionSound, thumpSound, knifeHitSound;
    public static Sound clockHitSound, alarmSound, ticktackSound, clickSound, woodCreakSound, biteSound, ballHitSound;
    public static Sound losingSound, winningSound, getPrizeSound;
    public static Sound zombieMoan1Sound;

    /* Music */
    public static Music marketMusic;


    public void loadSplashScreen () {

        try {
            ITexture texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/menu/splash_bg.jpg", TextureOptions.BILINEAR);
            splashBgTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/menu/splash_title.png", TextureOptions.BILINEAR);
            splashTitleTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();
        } catch (IOException e) {
            Debug.e(e);
        }
    }

    public void unloadSplashScreen () {
    }

    public void loadMenuScreen () {

        try {
            ITexture texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/menu/splash_bg.jpg", TextureOptions.BILINEAR);
            splashBgTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/menu/splash_title.png", TextureOptions.BILINEAR);
            splashTitleTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/menu/option_new.png", TextureOptions.BILINEAR);
            optionNewTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/menu/option_continue.png", TextureOptions.BILINEAR);
            optionContinueTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            try {
                texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/menu/music_icon.png", TextureOptions.BILINEAR);
                musicIconTexture =  TextureRegionFactory.extractTiledFromTexture(texture, 2, 1);
                texture.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            /* sounds */
            clickSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "click.ogg");

            /* Fonts */
            defaultSmallFont = FontFactory.create(activity.getFontManager(), activity.getTextureManager(), 256, 256, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 28, Color.WHITE);
            defaultSmallFont.load();

        } catch (IOException e) {
            Debug.e(e);
        }
    }

    public void unloadMenuScreen () {
        splashBgTexture.getTexture().unload();
        splashTitleTexture.getTexture().unload();
        optionNewTexture.getTexture().unload();
        optionContinueTexture.getTexture().unload();
        musicIconTexture.getTexture().unload();
        defaultSmallFont.unload();
    }

    public void loadGameResources() {


        /* Atlas textures */

        BuildableBitmapTextureAtlas bitmapTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 800, 800, TextureOptions.BILINEAR);
        zombieAppleTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(bitmapTextureAtlas, activity, "entities/apple_sheet.png", 4, 5);

        try {
            bitmapTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
            bitmapTextureAtlas.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            Debug.e(e);
        }


        bitmapTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
        zombiePearTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(bitmapTextureAtlas, activity, "entities/pear_sheet.png", 4, 5);

        try {
            bitmapTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
            bitmapTextureAtlas.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            Debug.e(e);
        }

        bitmapTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
        zombieBananaTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(bitmapTextureAtlas, activity, "entities/banana_sheet.png", 4, 5);

        try {
            bitmapTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
            bitmapTextureAtlas.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            Debug.e(e);
        }

        bitmapTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 2048, 2048, TextureOptions.BILINEAR);
        zombieWatermelonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(bitmapTextureAtlas, activity, "entities/watermelon_sheet.png", 4, 5);

        try {
            bitmapTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
            bitmapTextureAtlas.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            Debug.e(e);
        }

        bitmapTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
        zombieGrapeTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(bitmapTextureAtlas, activity, "entities/grape_sheet.png", 4, 4);

        try {
            bitmapTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
            bitmapTextureAtlas.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            Debug.e(e);
        }


        /*Tiled textures */


        try {
            ITexture texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/cuttlery.png");
            cuttleryTexture =  TextureRegionFactory.extractTiledFromTexture(texture, 4, 1);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/hud/items.png",TextureOptions.BILINEAR);
            itemsTexture =  TextureRegionFactory.extractTiledFromTexture(texture, 3, 1);
            texture.load();
        } catch (IOException e) {
            e.printStackTrace();
        }




        /* Bitmap textures */
        ITexture texture;
        try {

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/menu/hint.jpg", TextureOptions.BILINEAR);
            hintScreenTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/level/market_bg_small.png", BitmapTextureFormat.RGBA_8888);
            marketBgTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/level/market_fg_small.png", BitmapTextureFormat.RGBA_8888);
            marketFgTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/level/sky_small.png", BitmapTextureFormat.RGB_565);
            skyTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/level/clouds_small.png", BitmapTextureFormat.RGBA_4444);
            cloudsTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/level/ground.png", BitmapTextureFormat.RGB_565);
            groundTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/box_small.png");
            boxSmallTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/box_large.png");
            boxLargeTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/splat.png", BitmapTextureFormat.RGBA_4444);
            splatTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/explosion.png", BitmapTextureFormat.RGBA_4444);
            explosionTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/bottle.png");
            bottleTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/object_bit.png", BitmapTextureFormat.RGBA_4444);
            objectBitTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/fly.png", BitmapTextureFormat.RGBA_4444);
            flyTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/ball.png");
            ballTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/watering_can.png");
            wateringCanTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/pot.png");
            potTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/dynamite.png");
            dynamiteTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/clock.png");
            clockTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/hud/shot_button.png");
            shotButtonTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/hud/dot.png", BitmapTextureFormat.RGBA_4444);
            dotTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/hud/apple_icon.png");
            appleIconTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/hud/apple_rotten_icon.png");
            appleRottenIconTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/hud/power_bar_frame.png", BitmapTextureFormat.RGBA_4444);
            powerSliderFrameTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/hud/power_bar_bg.png");
            powerSliderBgTexture = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            /* Apple bits */

            fruitBitBaseTexture  = new ITextureRegion [2];

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/fruit_bit_a_base.png", BitmapTextureFormat.RGBA_4444);
            fruitBitBaseTexture [0] = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/fruit_bit_b_base.png", BitmapTextureFormat.RGBA_4444);
            fruitBitBaseTexture [1] = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            fruitBitTopTexture  = new ITextureRegion [2];

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/fruit_bit_a_top.png", BitmapTextureFormat.RGBA_4444);
            fruitBitTopTexture [0] = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/fruit_bit_b_top.png", BitmapTextureFormat.RGBA_4444);
            fruitBitTopTexture [1] = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            /* Onagre */

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/onagre_body_c.png", TextureOptions.BILINEAR);
            onagreBody = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/onagre_arm_c.png", TextureOptions.BILINEAR);
            onagreArm = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/onagre_wheel_c.png", TextureOptions.BILINEAR);
            onagreWheel = TextureRegionFactory.extractFromTexture(texture);
            texture.load();

            texture = new AssetBitmapTexture(activity.getTextureManager(), activity.getAssets(), "gfx/entities/onagre_tire_c.png", TextureOptions.BILINEAR);
            onagreTire = TextureRegionFactory.extractFromTexture(texture);
            texture.load();


        } catch (IOException e) {
            Debug.e(e);
        }

        /* Fonts */

        grinchedLargeFont = FontFactory.createFromAsset(activity.getFontManager(), activity.getTextureManager(), 512, 256,
                ResourcesMgr.getInstance().activity.getAssets(), "fonts/GrinchedRegular.otf", 70f, true, Color.WHITE);
        grinchedLargeFont.load();

        grinchedMediumFont = FontFactory.createFromAsset(activity.getFontManager(), activity.getTextureManager(), 512, 256,
                ResourcesMgr.getInstance().activity.getAssets(), "fonts/GrinchedRegular.otf", 55f, true, Color.WHITE);
        grinchedMediumFont.load();

        grinchedSmallFont = FontFactory.createFromAsset(activity.getFontManager(), activity.getTextureManager(), 256, 256,
                ResourcesMgr.getInstance().activity.getAssets(), "fonts/GrinchedRegular.otf", 30f, true, Color.WHITE);
        grinchedSmallFont.load();

        defaultSmallFont = FontFactory.create(activity.getFontManager(), activity.getTextureManager(), 256, 256, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 28, Color.WHITE);
        defaultSmallFont.load();

        /** Sounds */
        try {
            boxCrashSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "wooden_break.ogg");
            bottleCrashSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "glass_break.ogg");
            potCrashSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "pot_break.ogg");
            wateringCanCrashSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "metal_crash.ogg");
            explosionSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "explosion.ogg");
            thumpSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "thump.ogg");
            knifeHitSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "knife_hit.ogg");
            clockHitSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "clock_hit.ogg");
            alarmSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "alarm.ogg");
            ticktackSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "ticktack.ogg");
            clickSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "click.ogg");
            woodCreakSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "wood_creak.ogg");
            losingSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "losing_sound.ogg");
            winningSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "winning_sound.ogg");
            biteSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "bite.ogg");
            ballHitSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "ball_hit.ogg");
            getPrizeSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "get_prize.ogg");

            zombieMoan1Sound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "zombie_1.ogg");
        } catch (IOException e) {
            e.printStackTrace();
        }

        /** Sounds */
        try {
            marketMusic = MusicFactory.createMusicFromAsset(activity.getMusicManager(), activity, "music_market.ogg");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void unloadGameResources() {
        /* TODO pendiente */
    }


    public static void initialize (Engine engine, GameActivity ga, SmartCamera camera, VertexBufferObjectManager vbom) {
        getInstance().engine = engine;
        getInstance().activity = ga;
        getInstance().camera = camera;
        getInstance().vertexBufferObjectManager = vbom;
        getInstance().gameOptions = new GameOptions();

        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
        SoundFactory.setAssetBasePath("sfx/");
        MusicFactory.setAssetBasePath("mfx/");
    }

    public static ResourcesMgr getInstance() {
        return INSTANCE;
    }
}
