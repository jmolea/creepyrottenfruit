package com.crf.creepyrottenfruit.core;

import android.app.Activity;

import com.crf.creepyrottenfruit.camera.SmartCamera;
import com.crf.creepyrottenfruit.scenes.SceneMgr;

import org.andengine.engine.Engine;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/*
 * Created by Jose Maria Olea on 30/12/2017.
 */

public abstract class BaseScene extends Scene {

    protected Engine engine;
    protected Activity activity;
    protected ResourcesMgr resourcesMgr;
    protected VertexBufferObjectManager vertexBufferObjectManager;
    protected SmartCamera camera;

    public BaseScene () {
        this.resourcesMgr = ResourcesMgr.getInstance();
        this.engine = resourcesMgr.engine;
        this.activity = resourcesMgr.activity;
        this.vertexBufferObjectManager = resourcesMgr.vertexBufferObjectManager;
        this.camera = resourcesMgr.camera;
    }

    public abstract void createScene();

    public abstract void onBackKeyPressed();

    public abstract SceneMgr.SceneType getSceneType ();

    public abstract void disposeScene();

    public abstract void pause();

    public abstract void resume();

}
