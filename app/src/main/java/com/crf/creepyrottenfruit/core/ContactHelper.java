package com.crf.creepyrottenfruit.core;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.crf.creepyrottenfruit.entities.PhysicalObject;

/*
 * Created by Jose Maria Olea on 05/01/2018.
 */

public class ContactHelper {

    // ====================================================
    // ENUM
    // ====================================================

    // ====================================================
    // CONSTANTS
    // ====================================================

    // ====================================================
    // VARIABLES
    // ====================================================

    // ====================================================
    // CONSTRUCTOR
    // ====================================================

    // ====================================================
    // METHODS
    // ====================================================

    public static ContactListener PHYS_OBJECT_CONTACT_LISTENER = new ContactListener() {
        @Override
        public void beginContact(Contact contact) {
            PhysicalObject<?> physicsObjectA = (PhysicalObject<?>) contact.getFixtureA().getBody().getUserData();
            PhysicalObject<?> physicsObjectB = (PhysicalObject<?>) contact.getFixtureB().getBody().getUserData();

            if(physicsObjectA != null){
                physicsObjectA.beginContact(contact);
            }
            if(physicsObjectB != null){
                physicsObjectB.beginContact(contact);
            }
        }
        @Override
        public void endContact(Contact contact) {
            PhysicalObject<?> physicsObjectA = (PhysicalObject<?>) contact.getFixtureA().getBody().getUserData();
            PhysicalObject<?> physicsObjectB = (PhysicalObject<?>) contact.getFixtureB().getBody().getUserData();

            if(physicsObjectA != null){
                physicsObjectA.endContact(contact);
            }
            if(physicsObjectB != null){
                physicsObjectB.endContact(contact);
            }
        }
        @Override
        public void preSolve(Contact contact, Manifold oldManifold) {
            PhysicalObject<?> physicsObjectA = (PhysicalObject<?>) contact.getFixtureA().getBody().getUserData();
            PhysicalObject<?> physicsObjectB = (PhysicalObject<?>) contact.getFixtureB().getBody().getUserData();

            if(physicsObjectA != null){
                physicsObjectA.preSolve(contact, oldManifold);
            }
            if(physicsObjectB != null){
                physicsObjectB.preSolve(contact, oldManifold);
            }
        }
        @Override
        public void postSolve(Contact contact, ContactImpulse impulse) {
            PhysicalObject<?> physicsObjectA = (PhysicalObject<?>) contact.getFixtureA().getBody().getUserData();
            PhysicalObject<?> physicsObjectB = (PhysicalObject<?>) contact.getFixtureB().getBody().getUserData();

            if(physicsObjectA != null){
                physicsObjectA.postSolve(contact, impulse);
            }
            if(physicsObjectB != null){
                physicsObjectB.postSolve(contact, impulse);
            }
        }
    };
}
