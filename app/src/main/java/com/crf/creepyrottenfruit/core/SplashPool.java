package com.crf.creepyrottenfruit.core;

/*
 * Created by Jose Maria Olea on 31/01/2018.
 */

import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;
import org.andengine.util.adt.pool.GenericPool;

import java.util.ArrayList;
import java.util.Iterator;

public class SplashPool extends GenericPool<Sprite> implements IUpdateHandler{

    // ====================================================
    // CONSTANTS
    // ====================================================

    private final float DECAY_TIME = 1f;

    // ====================================================
    // VARIABLES
    // ====================================================

    ITextureRegion mTextureRegion;
    VertexBufferObjectManager mVertexBufferObjectManager;
    SpritePool splashPool;
    GameScene scene;
    ArrayList<Sprite> activeSplashes;

    // ====================================================
    // CONSTRUCTOR
    // ====================================================


    public SplashPool(GameScene scene, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        this.scene = scene;
        this.mTextureRegion = pTextureRegion;
        this.mVertexBufferObjectManager = pVertexBufferObjectManager;

        activeSplashes = new ArrayList<Sprite>();
    }

    // ====================================================
    // METHODS
    // ====================================================

    public void createSplash (float x, float y, Color color) {
        Sprite splash = obtainPoolItem(x,y, color);
        activeSplashes.add(splash);
    }


    @Override
    public void onUpdate(float pSecondsElapsed) {
        if (scene.gameState == GameScene.GameState.PLAYING) {
            Iterator it = activeSplashes.iterator();
            while (it.hasNext()) {
                Sprite s = (Sprite) it.next();
                float alpha = s.getAlpha() - pSecondsElapsed / DECAY_TIME;
                if (alpha <= 0) {
                    it.remove();
                    recyclePoolItem(s);
                } else {
                    s.setAlpha(alpha);
                }
            }
        }
    }

    @Override
    public void reset() {

    }

    /* obtainPoolItem handles the re-initialization of every object being
     obtained from the pool. In the case of a sprite pool, some general
     method calls would be to reposition the sprite, as well as set visible
     and allow updates within the update thread */
    public synchronized Sprite obtainPoolItem(final float pX, final float pY, Color color) {
        Sprite sprite = super.obtainPoolItem();

        sprite.setPosition(pX, pY);
        sprite.setColor(color);
        sprite.setAlpha(.99f);
        sprite.setRotation((float) (Math.random() * 2 * Math.PI));
        sprite.setScale(.8f);

        sprite.setVisible(true);
        sprite.setIgnoreUpdate(false);

        if (!sprite.hasParent()) {
            scene.getChildByIndex(Constants.LAYER_EFFECTS).attachChild(sprite);
        }
        return sprite;
    }

    /* onAllocatePoolItem handles the allocation of new activeSplashes in the
     event that we're attempting to obtain a new item while all pool
     items are currently in use */
    @Override
    protected Sprite onAllocatePoolItem() {
        return new Sprite(0, 0, this.mTextureRegion, this.mVertexBufferObjectManager);
    }

    /* onHandleRecycleItem is called when we use the recyclePoolItem() method.
     This method should handle the uninitializing of the object being recycled.
     In the case of a sprite pool, some methods that should be called include
     setting visibility to false, ignoring updates, and clearing modifiers and handlers */
    @Override
    protected void onHandleRecycleItem(Sprite pItem) {
        super.onHandleRecycleItem(pItem);

        pItem.setVisible(false);
        pItem.setIgnoreUpdate(true);
        pItem.clearEntityModifiers();
        pItem.clearUpdateHandlers();
    }
}
