package com.crf.creepyrottenfruit.core;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.crf.creepyrottenfruit.Constants;

import org.andengine.extension.physics.box2d.PhysicsWorld;


/*
 * Created by Jose Maria Olea on 05/01/2018.
 */

public class CompressedPhysicsWorld extends PhysicsWorld {

    // ====================================================
    // VARIABLES
    // ====================================================
    private float mTimeCompression=1f;

    // ====================================================
    // CONSTRUCTOR
    // ====================================================

    public CompressedPhysicsWorld(final Vector2 pGravity, final boolean pAllowSleep, float timeCompression, final int pVelocityIterations, final int pPositionIterations) {
        super( pGravity, pAllowSleep, pVelocityIterations, pPositionIterations);
        mTimeCompression = timeCompression;
    }

    public CompressedPhysicsWorld(final Vector2 pGravity, final boolean pAllowSleep) {
        super( pGravity, pAllowSleep);
    }

    public CompressedPhysicsWorld(final Vector2 pGravity, final boolean pAllowSleep, final int pVelocityIterations, final int pPositionIterations) {
        super(pGravity, pAllowSleep,pVelocityIterations,pPositionIterations);
    }

    // ====================================================
    // METHODS
    // ====================================================

    @Override
    public void onUpdate(final float pSecondsElapsed) {
        super.onUpdate(pSecondsElapsed * mTimeCompression);
    }

    public float getTimeCompression() {
        return mTimeCompression;
    }

    public void setTimeCompression(float mTimeCompression) {
        this.mTimeCompression = mTimeCompression;
    }


}
