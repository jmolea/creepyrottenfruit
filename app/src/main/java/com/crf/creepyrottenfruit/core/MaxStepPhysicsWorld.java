package com.crf.creepyrottenfruit.core;

import com.badlogic.gdx.math.Vector2;

import org.andengine.extension.physics.box2d.PhysicsWorld;

/*
 * Created by Jose Maria Olea on 31/01/2018.
 */

public class MaxStepPhysicsWorld extends PhysicsWorld {
    public static final int STEPSPERSECOND_DEFAULT = 60;
    private final float mStepLength;

    public MaxStepPhysicsWorld(final int pStepsPerSecond, final Vector2 pGravity, final boolean pAllowSleep, final int pVelocityIterations, final int pPositionIterations) {
        super(pGravity, pAllowSleep, pVelocityIterations, pPositionIterations);
        this.mStepLength = 2f / pStepsPerSecond;
    }

    @Override
    public void onUpdate(final float pSecondsElapsed) {
        float secondsElapsed = pSecondsElapsed *2;
        this.mPhysicsConnectorManager.onUpdate(mStepLength);

        float stepLength = secondsElapsed;
        if(secondsElapsed>= this.mStepLength){
            stepLength = this.mStepLength;
        }
        this.mWorld.step(stepLength, this.mVelocityIterations, this.mPositionIterations);

        this.mPhysicsConnectorManager.onUpdate(secondsElapsed);
    }

}