package com.crf.creepyrottenfruit.levels;

import com.badlogic.gdx.math.Vector2;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.entity.sprite.Sprite;
import org.andengine.util.math.MathUtils;

/*
 * Created by Jose Maria Olea on 29/01/2018.
 */

public class Fly extends Sprite{

    // ====================================================
    // CONSTANTS
    // ====================================================
    float SPEED = 150f;
    float DISTANCE2 = 150 * 150;

    // ====================================================
    // VARIABLES
    // ====================================================
    float originX, originY;
    float dX, dY;
    float direction;


    // ====================================================
    // CONSTRUCTOR
    // ====================================================
    public Fly (float x, float y, GameScene scene)
    {
        super(x, y, ResourcesMgr.flyTexture, ResourcesMgr.getInstance().vertexBufferObjectManager);
        this.setScale(.75f);
        this.setCullingEnabled(true);
        this.originX = x;
        this.originY = y;
        this.direction = MathUtils.random(0,(float)Math.PI *2);


        scene.getChildByIndex(Constants.LAYER_EFFECTS).attachChild(this);
    }


    public void update (final float pSecondsElapsed) {
        this.setX(getX()+dX * pSecondsElapsed);
        this.setY(getY()+dY * pSecondsElapsed);
        this.direction = direction + (MathUtils.random(-.5f,.5f));
        this.dX = SPEED *  (float) Math.cos(direction);
        this.dY = SPEED *  (float) Math.sin(direction);
        Vector2 radius = new Vector2(originX - getX(), originY - getY());
        if (radius.len2() > DISTANCE2) {
            direction = MathUtils.atan2(radius.y, radius.x);
        }
    }

    public void destroy () {
        this.detachSelf();
    }


}
