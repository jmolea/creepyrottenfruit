package com.crf.creepyrottenfruit.levels;

import com.crf.creepyrottenfruit.scenes.GameScene;

import org.andengine.engine.handler.IUpdateHandler;

import java.util.ArrayList;

/**
 * Created by Jose Maria Olea on 29/01/2018.
 */

public class Swarm implements IUpdateHandler{

    // ====================================================
    // CONSTANTS
    // ====================================================
    float AMOUNT  = 10;

    // ====================================================
    // VARIABLES
    // ====================================================
    private GameScene scene;
    private ArrayList<Fly> flies;

    public Swarm (float x, float y, GameScene scene) {
        this.scene = scene;
        flies = new ArrayList<Fly>();
        for (int i = 0;i<AMOUNT; i++) {
            flies.add(new Fly(x,y,scene));
        }
    }

    @Override
    public void onUpdate(float pSecondsElapsed) {
        if (scene.gameState == GameScene.GameState.PLAYING) {
            for (Fly fly : flies) {
                fly.update(pSecondsElapsed);
            }
        }
    }

    @Override
    public void reset() {

    }

    public void destroy () {
        for (Fly fly: flies) {
            fly.destroy();
            fly = null;
        }
        this.flies = null;
    }
}
