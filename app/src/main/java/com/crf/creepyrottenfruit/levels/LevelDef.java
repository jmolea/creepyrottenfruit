package com.crf.creepyrottenfruit.levels;

/*
 * Created by Jose Maria Olea on 06/01/2018.
 */

public class LevelDef {

    public static final LevelDef[] AvailableLevels = new LevelDef[] {
            new LevelDef( 5,0,0,0, 0,7, -.25f,6, 1,true,false,false),
            new LevelDef(8,4, 0,0,0,6.5f,-.25f,6,1,true,false,false),
            new LevelDef(8,4,2, 0,0,6f,-.25f,7,1,true,false,false),
            new LevelDef( 9,6, 4, 1,0,6f,-.25f,7,2,true,true,false),
            new LevelDef( 10,6, 5,2,0,5,-.25f,8,2,true,true,false),
            new LevelDef( 10,0,0,0, 3,5, -.25f,8,1,true,true,false),
            new LevelDef(12,8, 0,0,4,5f,-.25f,9,2,true,true,true),
            new LevelDef(12,8,4, 0,5,4.5f,-.25f,9,3,true,true,true),
            new LevelDef( 12,10, 6, 2,6,4.5f,-.25f,10,3,true,true,true),
            new LevelDef( 12,12, 10,3,8,4,-.25f,10,3,true,true,true)
    };

    public int amountEnemies;
    public int amountApple;
    public int amountPear;
    public int amountBanana;
    public int amountWatermelon;
    public int amountGrape;
    public boolean itemDynamite, itemCuttlery, itemClock;

    public int hordes;

    public final float initialSpawnTime;
    public final float spawnTimeDelta;

    public final int killsPerItem;


    public LevelDef(int apples, int pears, int bananas, int watermelon, int grapes, float initialSpawnTime, float spawnTimeDelta, int killsPerItem, int hordes, boolean itemDynamite, boolean itemCuttlery, boolean itemClock) {
        this.amountApple = apples;
        this.amountPear = pears;
        this.amountBanana = bananas;
        this.amountWatermelon = watermelon;
        this.amountGrape =grapes;
        this.initialSpawnTime = initialSpawnTime;
        this.spawnTimeDelta = spawnTimeDelta;
        this.killsPerItem = killsPerItem;
        this.hordes = hordes;
        this.itemDynamite = itemDynamite;
        this.itemCuttlery = itemCuttlery;
        this.itemClock = itemClock;
    }


}
