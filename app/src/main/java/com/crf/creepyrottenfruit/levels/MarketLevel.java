package com.crf.creepyrottenfruit.levels;

/*
 * Created by Jose Maria Olea on 29/12/2017.
 */

public class MarketLevel  {


    public static float WIDTH = 3000;
    public static float HEIGHT = 1250;

    public static float BOUND_X_MIN = -WIDTH / 2;
    public static float BOUND_Y_MIN = -100;
    public static float BOUND_X_MAX = WIDTH / 2;
    public static float BOUND_Y_MAX = 1200;

    public static float MAX_ZOOM = 1.2f;
    public static float INITIAL_ZOOM = .8f;
    public static float MIN_ZOOM = .55f;//.55f;

}
