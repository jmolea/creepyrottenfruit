package com.crf.creepyrottenfruit.scenes;

import android.util.Log;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.crf.creepyrottenfruit.core.ContactHelper;
import com.crf.creepyrottenfruit.BackgroundFactory;
import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.BaseScene;
import com.crf.creepyrottenfruit.core.MaxStepPhysicsWorld;
import com.crf.creepyrottenfruit.core.ResourcesMgr;
import com.crf.creepyrottenfruit.core.SplashPool;
import com.crf.creepyrottenfruit.entities.DestructionArea;
import com.crf.creepyrottenfruit.entities.enemies.Enemy;
import com.crf.creepyrottenfruit.entities.FixedObject;
import com.crf.creepyrottenfruit.entities.Goal;
import com.crf.creepyrottenfruit.entities.Onagre;
import com.crf.creepyrottenfruit.levels.Swarm;
import com.crf.creepyrottenfruit.entities.enemies.*;
import com.crf.creepyrottenfruit.entities.projectiles.Clock;
import com.crf.creepyrottenfruit.hud.FadingText;
import com.crf.creepyrottenfruit.hud.ItemsBar;
import com.crf.creepyrottenfruit.hud.PowerSlider;
import com.crf.creepyrottenfruit.hud.ShadowText;
import com.crf.creepyrottenfruit.levels.LevelDef;
import com.crf.creepyrottenfruit.levels.MarketLevel;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.particle.SpriteParticleSystem;
import org.andengine.entity.particle.emitter.PointParticleEmitter;
import org.andengine.entity.particle.initializer.AlphaParticleInitializer;
import org.andengine.entity.particle.initializer.ColorParticleInitializer;
import org.andengine.entity.particle.initializer.RotationParticleInitializer;
import org.andengine.entity.particle.modifier.AlphaParticleModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.entity.util.FPSCounter;
import org.andengine.extension.debugdraw.DebugRenderer;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.input.touch.TouchEvent;
import org.andengine.input.touch.detector.PinchZoomDetector;
import org.andengine.input.touch.detector.ScrollDetector;
import org.andengine.input.touch.detector.SurfaceScrollDetector;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.adt.color.Color;
import org.andengine.util.math.MathUtils;
import org.andengine.opengl.texture.region.ITextureRegion;

import java.util.ArrayList;
import java.util.Collections;

/*
 * Created by Jose Maria Olea on 30/12/2017.
 */

public class GameScene extends BaseScene implements IOnSceneTouchListener, ScrollDetector.IScrollDetectorListener, PinchZoomDetector.IPinchZoomDetectorListener{


    // ====================================================
    // ENUM
    // ====================================================
    public enum GameState {
        PLAYING, PAUSED
    }


    // ====================================================
    // CONSTANTS
    // ====================================================

    private final float ENEMY_SPAWN_POSITION_X = MarketLevel.WIDTH / 2 + 50;
    private final float ENEMY_SPAWN_POSITION_Y = 200;
    private final float ENEMY_GOAL_POSITION_X = - MarketLevel.WIDTH / 2 + 250;
    private final float ONAGER_SPAWN_POSITION_X = - MarketLevel.WIDTH / 2 + 250;
    private final int PLAYER_INITIAL_LIFES = 3;
    private final float POWER_SLIDER_POSITION_X = 50;
    private final float POWER_SLIDER_POSITION_Y = Constants.CAMERA_HEIGHT / 2;
    private final float LIVES_BAR_POSITION_X = 150;
    private final float LIVES_BAR_POSITION_Y = Constants.CAMERA_HEIGHT-100;
    private final float ITEM_LIFE_WIDTH = 75;

    private final float ITEMS_BAR_POSITION_X = Constants.CAMERA_WIDTH - 100;
    private final float ITEMS_BAR_POSITION_Y = Constants.CAMERA_HEIGHT/2 - 50;
    private final float COMBO_TIME_WINDOW = 3f;
    public final int MAX_ITEMS = 3;
    private final float PAUSE_BETWEEN_HORDES_TIME = 3f;
    private final float ENEMY_MINIMUM_SPAWN_TIME = 1f;

    private final Color APPLE_SPLAT_COLOR = new Color (228f/255,182f/255,154f/255);
    private final Color SCORE_TEXT_COLOR = new Color(255f/255,184f/255,33f/255);


    /* collision categories */
    public static final short CATEGORYBIT_ENEMIES = 1;
    public static final short CATEGORYBIT_KNIVES = 2;
    public static final short CATEGORYBIT_ONAGRE = 4;
    public static final short CATEGORYBIT_PROJECTILES = 8;
    public static final short CATEGORYBIT_GROUND = 16;
    public static final short CATEGORYBIT_GOAL = 32;

    /* And what should collide with what. */
    public static final short MASKBITS_ENEMIES = CATEGORYBIT_PROJECTILES + CATEGORYBIT_KNIVES + CATEGORYBIT_GROUND + CATEGORYBIT_GOAL;
    public static final short MASKBITS_KNIVES = CATEGORYBIT_ENEMIES + CATEGORYBIT_PROJECTILES + CATEGORYBIT_GROUND;
    public static final short MASKBITS_ONAGRE = CATEGORYBIT_GROUND;
    public static final short MASKBITS_PROJECTILES = CATEGORYBIT_ENEMIES + CATEGORYBIT_PROJECTILES + CATEGORYBIT_KNIVES + CATEGORYBIT_GROUND;
    public static final short MASKBITS_GROUND = CATEGORYBIT_ENEMIES + CATEGORYBIT_PROJECTILES + CATEGORYBIT_KNIVES + CATEGORYBIT_ONAGRE;
    public static final short MASKBITS_GOAL = CATEGORYBIT_ENEMIES;

    // ====================================================
    // VARIABLES
    // ====================================================
    public PhysicsWorld mPhysicsWorld;

    //HUD
    private HUD hud;
    private Entity livesBar;
    public boolean livesBarMarkedToUpdate;
    private PowerSlider powerSlider;
    private ShadowText enemiesLeftText;
    public ItemsBar itemsBar;
    private Text nextLevelText;

    final FPSCounter fpsCounter =  new FPSCounter();
    private Text fpsText;

    private SurfaceScrollDetector mScrollDetector;
    private PinchZoomDetector mPinchZoomDetector;
    private float mPinchZoomStartedCameraZoomFactor;

    private boolean levelInitialized = false;
    private Vector2 lastCameraPosition;
    private float lastCameraZoomFactor;

    public Onagre onagre;

    private int currentLevel;
    public LevelDef levelDef;

    public GameState gameState;
    private float enemySpawnTime, spawnTimer;
    public int mLivesLeft;
    public int enemiesLeft,totalEnemies, enemiesKilled, enemiesSpawned;
    private float comboTimeCounter;
    private int comboKillsCounter;

    public Clock ringinClock;

    private ArrayList<String> enemiesList;
    private boolean pauseBetweenHordes=false;
    public SplashPool splashManager;



    // ====================================================
    //  DEFAULT CONSTRUCTOR
    // ====================================================
    public GameScene() {
        super();
        resetGame();
    }

    /** Restart in the same level after losing */
    private void resetLevel( int level) {
        itemsBar = null; // Lose all items
        loadLevel(level);
    }

    /** Start a new game from scratch from the menu */
    public void resetGame () {
        // Reset Score
        itemsBar = null;

        // Show initial hint screen
        Sprite hint = new Sprite(Constants.CAMERA_WIDTH/2,Constants.CAMERA_HEIGHT/2,ResourcesMgr.hintScreenTexture, ResourcesMgr.getInstance().vertexBufferObjectManager) {
            @Override
            public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionDown()) {
                    this.detachSelf();
                    gameState = GameState.PLAYING;
                    // Create level
                    loadLevel(Constants.START_AT_LEVEL);

                }
                return true;
            }
        };
        this.attachChild(hint);
        this.registerTouchArea(hint);

        gameState = GameState.PAUSED;
    }

    /** Load a level without reseting the item bar */
    private void loadLevel( int level ) {
        levelInitialized = false;
        currentLevel = level;

        GameScene.this.detachChildren();
        GameScene.this.clearEntityModifiers();
        GameScene.this.clearTouchAreas();
        GameScene.this.clearUpdateHandlers();
        engine.clearUpdateHandlers();
        if (mPhysicsWorld!= null)
            mPhysicsWorld.clearPhysicsConnectors();
        if (hud != null) {
            hud.detachChildren();
        }
        camera.setHUD(null);

        createScene();

    }

    @Override
    public void createScene() {
        levelDef = LevelDef.AvailableLevels[currentLevel];

        createLayers();
        createPhysics();
        createHUD();
        createLevel();
        initializeCamera();
        initializeMusic();
        levelInitialized = true;

    }

    @Override
    public void onBackKeyPressed() {

    }

    @Override
    public SceneMgr.SceneType getSceneType() {
        return SceneMgr.SceneType.SCENE_GAME;
    }

    @Override
    public void disposeScene() {
        // detach and unload the scene.

        engine.runOnUpdateThread(new Runnable() {
            @Override
            public void run() {
                camera.reset();
                ResourcesMgr.marketMusic.stop();

                GameScene.this.detachChildren();
                GameScene.this.clearEntityModifiers();
                GameScene.this.clearTouchAreas();
                GameScene.this.clearUpdateHandlers();
                mPhysicsWorld.clearPhysicsConnectors();

            }
        });
    }

    private void createLayers() {
        Log.d(Constants.TAG, "createLayers()");

        /* Create parallax background  */
        Background marketBg = BackgroundFactory.createMarketBackground();
        setBackground(marketBg);

         /* Create Scene layers */
        attachChild(new Entity()); // bg
        attachChild(new Entity()); // items
        attachChild(new Entity()); // middle
        attachChild(new Entity()); // effects
        attachChild(new Entity()); // debug

    }

    private void createHUD () {
        Log.d(Constants.TAG, "createHud()");
        hud = new HUD();

        /* Power slider */
        powerSlider = new PowerSlider(POWER_SLIDER_POSITION_X, POWER_SLIDER_POSITION_Y, 120, (float) Constants.CAMERA_HEIGHT, this);
        hud.attachChild(powerSlider);
        hud.registerTouchArea(powerSlider);
        hud.setTouchAreaBindingOnActionDownEnabled(true);

        /* Enemies left text */
        enemiesLeftText = new ShadowText(Constants.CAMERA_WIDTH - 200, Constants.CAMERA_HEIGHT - 100, ResourcesMgr.grinchedMediumFont, this.levelDef.amountEnemies + " enemies left", new TextOptions(HorizontalAlign.LEFT), vertexBufferObjectManager);
        enemiesLeftText.setColor(SCORE_TEXT_COLOR);
        hud.attachChild(enemiesLeftText);

        /* Lives bar */
        livesBar = new Entity();
        hud.attachChild(livesBar);

        /* Items selector */
        if (itemsBar == null) { // Respect the existing items if coming from previous level
            itemsBar = new ItemsBar(ITEMS_BAR_POSITION_X, ITEMS_BAR_POSITION_Y, levelDef.killsPerItem, this);
        }
        hud.attachChild(itemsBar);
        hud.registerUpdateHandler(itemsBar);
        hud.registerTouchArea(itemsBar);


        /* FPS counter */
        if (Constants.DEBUG_MODE || Constants.SHOW_FPS) {
            engine.registerUpdateHandler(fpsCounter);
            fpsText = new Text(600, LIVES_BAR_POSITION_Y + 50, ResourcesMgr.defaultSmallFont, "FPS:", "FPS: XXXXXXXXXX".length(), ResourcesMgr.getInstance().vertexBufferObjectManager);
            hud.attachChild(fpsText);
            registerUpdateHandler(new TimerHandler(1 / 20.0f, true, new ITimerCallback() {
                @Override
                public void onTimePassed(final TimerHandler pTimerHandler) {
                    float fps = fpsCounter.getFPS();
                    fpsText.setText( String.format("FPS: %.2f", fps));
                }
            }));
        }

    }

    public void createPhysics () {
        Log.d(Constants.TAG, "createPhysics()");
        //mPhysicsWorld = new CompressedPhysicsWorld(new Vector2(0, -10), false, Constants.PHYSICS_TIME_COMPRESSION, 16,8);
        mPhysicsWorld = new MaxStepPhysicsWorld(30,new Vector2(0, -10), false, 8,8);
        mPhysicsWorld.setContactListener(ContactHelper.PHYS_OBJECT_CONTACT_LISTENER);
        registerUpdateHandler(mPhysicsWorld);

    }


    public void createLevel() {
        Log.d(Constants.TAG, "createLevel()");

        /* Debug renderer */
        if (Constants.DEBUG_MODE) {
            DebugRenderer debug = new DebugRenderer(mPhysicsWorld, vertexBufferObjectManager);
            this.getChildByIndex(Constants.LAYER_DEBUG).attachChild(debug);
        }

        /* Populate scene*/

        // Ground
        FixedObject ground = new FixedObject();
        ground.mEntity = new Sprite(100, -100, ResourcesMgr.groundTexture, resourcesMgr.vertexBufferObjectManager);
        ground.mEntity.setWidth(MarketLevel.WIDTH + 250);
        ground.mEntity.setIgnoreUpdate(true);
        ground.mEntity.setHeight(200);
        ground.mBody = PhysicsFactory.createBoxBody(mPhysicsWorld, ground.mEntity, BodyDef.BodyType.StaticBody, PhysicsFactory.createFixtureDef(0, 0.0f, 0.9f, false, CATEGORYBIT_GROUND, MASKBITS_GROUND, (short)0));
        ground.mBody.setUserData(ground);
        this.getChildByIndex(Constants.LAYER_ITEMS).attachChild(ground.mEntity);

        FixedObject rightWall = new FixedObject();
        rightWall.mEntity = new Rectangle(MarketLevel.WIDTH/2+200,100,50,200,resourcesMgr.vertexBufferObjectManager);
        rightWall.mEntity.setIgnoreUpdate(true);
        rightWall.mBody = PhysicsFactory.createBoxBody(mPhysicsWorld, rightWall.mEntity, BodyDef.BodyType.StaticBody, PhysicsFactory.createFixtureDef(0, 0.0f, 0.9f, false, CATEGORYBIT_GROUND, MASKBITS_GROUND, (short)0));
        rightWall.mBody.setUserData(rightWall);
        this.getChildByIndex(Constants.LAYER_ITEMS).attachChild(rightWall.mEntity);

        // Onagre
        onagre = new Onagre (ONAGER_SPAWN_POSITION_X,142,  this);

        // Goal
        final float margin = 200;
        new Goal(ENEMY_GOAL_POSITION_X - 100,MarketLevel.HEIGHT/2,400,MarketLevel.HEIGHT, this);

        // Destruction areas: scenenary border. Destroys everything
        new DestructionArea(0,- margin,MarketLevel.WIDTH + margin * 2,5, this);
        new DestructionArea(0,MarketLevel.HEIGHT + margin, MarketLevel.WIDTH + margin * 2,5, this);
        new DestructionArea(- MarketLevel.WIDTH/2 - margin,MarketLevel.HEIGHT / 2 ,5,MarketLevel.HEIGHT + margin * 2, this);
        new DestructionArea(MarketLevel.WIDTH/2 + margin,MarketLevel.HEIGHT / 2 ,5,MarketLevel.HEIGHT + margin * 2, this);

        //Destruction areas: onagre space. Destroys only projectiles
        FixtureDef fixture = PhysicsFactory.createFixtureDef(0,0,0, true, CATEGORYBIT_ENEMIES, MASKBITS_ENEMIES, (short)0);
        new DestructionArea(ONAGER_SPAWN_POSITION_X + 50, 10, 200, 20, this, fixture);
        new DestructionArea(MarketLevel.WIDTH/2 + 100, 10, 200, 20, this, fixture);

        // Flies
        Swarm swarm = new Swarm(MarketLevel.WIDTH/2 - 100,275, this);
        this.registerUpdateHandler(swarm);

        // Splash manager
        splashManager = new SplashPool(this, ResourcesMgr.splatTexture, ResourcesMgr.getInstance().vertexBufferObjectManager);
        this.registerUpdateHandler(splashManager);

        /* Listeners */

        this.mScrollDetector = new SurfaceScrollDetector(this);
        this.mPinchZoomDetector = new PinchZoomDetector(this);
        setOnSceneTouchListener(this);
        setTouchAreaBindingOnActionDownEnabled(true);


        /* Initialize level logic */
        gameState = GameState.PLAYING;

        mLivesLeft = PLAYER_INITIAL_LIFES;
        enemiesSpawned = 0;

        enemySpawnTime = levelDef.initialSpawnTime;  //Initialize spawn timer
        spawnTimer = enemySpawnTime;
        enemiesList = createEnemiesList (levelDef);
        totalEnemies =  levelDef.amountApple + levelDef.amountPear + levelDef.amountBanana +levelDef.amountWatermelon + levelDef.amountGrape;
        enemiesLeft = totalEnemies;
        enemiesKilled = 0;
        livesBarMarkedToUpdate = true; // Update lives bar when start

        if (Constants.DEBUG_MODE) {
            engine.registerUpdateHandler(new TimerHandler(3f, true, new ITimerCallback() {
                @Override
                public void onTimePassed(TimerHandler pTimerHandler) {
                    showAllChildrenCount(GameScene.this);
                }
            }));
        }


        registerUpdateHandler(new IUpdateHandler() {
            @Override
            public void onUpdate(float pSecondsElapsed) {



                if (gameState == GameState.PLAYING){
                    // Check wining conditions
                    if (mLivesLeft <= 0) {
                        //you lose
                        gameState = GameState.PAUSED;
                        engine.registerUpdateHandler(new TimerHandler(2f, new ITimerCallback() {
                            @Override
                            public void onTimePassed(TimerHandler pTimerHandler) {
                                engine.unregisterUpdateHandler(pTimerHandler);
                                showLoseScreen ();
                            }
                        }));

                    } else if (enemiesLeft <= 0) {
                        //you win. Go to next level
                        gameState = GameState.PAUSED;
                        engine.registerUpdateHandler(new TimerHandler(2f, new ITimerCallback() {
                            @Override
                            public void onTimePassed(TimerHandler pTimerHandler) {
                                engine.unregisterUpdateHandler(pTimerHandler);
                                showWinScreen ();
                            }
                        }));
                    }

                    // Update HUD
                    enemiesLeftText.setText(enemiesLeft + " enemies left");
                    if (livesBarMarkedToUpdate) {
                        livesBar.detachChildren();
                        for (int i = 0; i < PLAYER_INITIAL_LIFES; i++ ) {
                            ITextureRegion iconTexture = i < mLivesLeft? ResourcesMgr.appleIconTexture: ResourcesMgr.appleRottenIconTexture;
                            Sprite icon = new Sprite(LIVES_BAR_POSITION_X + (i * ITEM_LIFE_WIDTH), LIVES_BAR_POSITION_Y, iconTexture, ResourcesMgr.getInstance().vertexBufferObjectManager);
                            icon.setRotation(-10f);
                            if (i >= mLivesLeft) {
                                icon.setAlpha(.6f);
                            }
                            livesBar.attachChild(icon);
                        }
                        livesBarMarkedToUpdate = false;
                    }

                    // Spawn check
                    if (enemiesList.size()>0 && !pauseBetweenHordes) {

                        spawnTimer -= pSecondsElapsed;

                        if(spawnTimer<= 0) {
                            Enemy newEnemy = null;
                            switch (enemiesList.get(0)) {
                                case "0":
                                    newEnemy = new ZombieApple(ENEMY_SPAWN_POSITION_X, ENEMY_SPAWN_POSITION_Y, GameScene.this);
                                    break;
                                case "1":
                                    newEnemy = new ZombiePear(ENEMY_SPAWN_POSITION_X, ENEMY_SPAWN_POSITION_Y, GameScene.this);
                                    break;
                                case "2":
                                    newEnemy = new ZombieBanana(ENEMY_SPAWN_POSITION_X, ENEMY_SPAWN_POSITION_Y, GameScene.this);
                                    break;
                                case "3":
                                    newEnemy = new ZombieWatermelon(ENEMY_SPAWN_POSITION_X, ENEMY_SPAWN_POSITION_Y, GameScene.this);
                                    break;
                                case "4":
                                    new ZombieGrape(ENEMY_SPAWN_POSITION_X, ENEMY_SPAWN_POSITION_Y, GameScene.this);
                                    break;
                                case "P":
                                    pauseBetweenHordes = true;
                                    engine.registerUpdateHandler(new TimerHandler(PAUSE_BETWEEN_HORDES_TIME, new ITimerCallback() {
                                        @Override
                                        public void onTimePassed(TimerHandler pTimerHandler) {
                                            engine.unregisterUpdateHandler(pTimerHandler);
                                            resetEnemySpawnTime();
                                            pauseBetweenHordes = false;
                                        }
                                    }));
                                    break;
                            }
                            if (newEnemy != null){
                                // Jump from the garbage bin
                                Vector2 impulse = new Vector2 (MathUtils.random(-15, -10), MathUtils.random(10, 30));
                                newEnemy.mBody.applyLinearImpulse(impulse, newEnemy.mBody.getWorldCenter());
                            }
                            enemiesList.remove(0);

                            enemySpawnTime += levelDef.spawnTimeDelta;
                            enemySpawnTime = Math.max(enemySpawnTime, ENEMY_MINIMUM_SPAWN_TIME);
                            spawnTimer = MathUtils.random(.5f, 1.5f) * enemySpawnTime;
                            enemiesSpawned ++;

                        }
                    }

                    // Combo counter
                    if (comboTimeCounter > 0) {
                        comboTimeCounter -= pSecondsElapsed;
                        if (comboTimeCounter <=0) {
                            comboTimeCounter = 0;
                            comboKillsCounter = 0;
                        }
                    }
                }
            }
            @Override public void reset() {}
        });
    }


    public void initializeCamera () {
        Log.d(Constants.TAG, "initializeCamera()");
        /* Define camera limits */
        camera = ResourcesMgr.getInstance().camera;

        camera.setBounds(MarketLevel.BOUND_X_MIN, MarketLevel.BOUND_Y_MIN, MarketLevel.BOUND_X_MAX, MarketLevel.BOUND_Y_MAX);
        camera.setBoundsEnabled(true);
        camera.setMinZoomFactor(MarketLevel.MIN_ZOOM);
        camera.setMaxZoomFactor(MarketLevel.MAX_ZOOM);
        camera.setMaxZoomFactorChange(.8f);
        camera.setLimitZoomEnabled(true);
        camera.setMaxVelocity(Constants.CAMERA_MAX_VELOCITY, Constants.CAMERA_MAX_VELOCITY);

        // If coming from a paused state, dont initialize the camera position/zoom
        if (!levelInitialized) {
            camera.setZoomFactor(MarketLevel.INITIAL_ZOOM);
            camera.setCenterDirect(onagre.body.mSprite.getX(), onagre.body.mSprite.getY());
            engine.registerUpdateHandler(new TimerHandler(2.5f, new ITimerCallback() {
                @Override
                public void onTimePassed(TimerHandler pTimerHandler) {
                    if (gameState != GameState.PAUSED) {
                        engine.unregisterUpdateHandler(pTimerHandler);
                        camera.setCenter(MarketLevel.WIDTH / 2, onagre.body.mSprite.getY());
                    }
                }
            }));
        }

        camera.setHUD(hud);

    }

    public void initializeMusic() {

        if (ResourcesMgr.getInstance().gameOptions.musicEnabled) {
            if (!ResourcesMgr.marketMusic.isPlaying()) {
                ResourcesMgr.marketMusic.setLooping(true);
                ResourcesMgr.marketMusic.setVolume(.5f);
                ResourcesMgr.marketMusic.play();
            } else {
                ResourcesMgr.marketMusic.resume();
            }
        }

    }


    @Override
    public boolean onSceneTouchEvent(final Scene pScene, final TouchEvent pSceneTouchEvent) {

        this.mPinchZoomDetector.onTouchEvent(pSceneTouchEvent);

        if(this.mPinchZoomDetector.isZooming()) {
            this.mScrollDetector.setEnabled(false);
        } else {
            if(pSceneTouchEvent.isActionDown()) {
                this.mScrollDetector.setEnabled(true);
            }
            this.mScrollDetector.onTouchEvent(pSceneTouchEvent);
        }

        return true;
    }

    @Override
    public void onScrollStarted(final ScrollDetector pScrollDetector, final int pPointerID, final float pDistanceX, final float pDistanceY) {
        Log.d(Constants.TAG, "onScrollStarted");
        resourcesMgr.camera.setChaseEntity(null);  // liberate camera
        resourcesMgr.camera.setMaxVelocity(5000,5000);
        final float zoomFactor = resourcesMgr.camera.getZoomFactor();
        resourcesMgr.camera.offsetCenter(-pDistanceX / zoomFactor, pDistanceY / zoomFactor);
    }

    @Override
    public void onScroll(final ScrollDetector pScrollDetector, final int pPointerID, final float pDistanceX, final float pDistanceY) {
        final float zoomFactor = resourcesMgr.camera.getZoomFactor();
        resourcesMgr.camera.offsetCenter(-pDistanceX / zoomFactor, pDistanceY / zoomFactor);
    }

    @Override
    public void onScrollFinished(final ScrollDetector pScollDetector, final int pPointerID, final float pDistanceX, final float pDistanceY) {
        float speedMultiplier = 2f;
        resourcesMgr.camera.setMaxVelocity(Constants.CAMERA_MAX_VELOCITY,Constants.CAMERA_MAX_VELOCITY);
        final float zoomFactor = resourcesMgr.camera.getZoomFactor() / speedMultiplier;
        resourcesMgr.camera.offsetCenter(-pDistanceX / zoomFactor, pDistanceY / zoomFactor);
    }


    @Override
    public void onPinchZoomStarted(final PinchZoomDetector pPinchZoomDetector, final TouchEvent pTouchEvent) {
        resourcesMgr.camera.setChaseEntity(null);  // liberate camera
        this.mPinchZoomStartedCameraZoomFactor = resourcesMgr.camera.getZoomFactor();
    }

    @Override
    public void onPinchZoom(final PinchZoomDetector pPinchZoomDetector, final TouchEvent pTouchEvent, final float pZoomFactor) {
        resourcesMgr.camera.setZoomFactor(this.mPinchZoomStartedCameraZoomFactor * pZoomFactor);
    }

    @Override
    public void onPinchZoomFinished(final PinchZoomDetector pPinchZoomDetector, final TouchEvent pTouchEvent, final float pZoomFactor) {
        resourcesMgr.camera.setZoomFactor(this.mPinchZoomStartedCameraZoomFactor * pZoomFactor);
    }

    public void removeLife () {
        if (mLivesLeft>0) {
            ResourcesMgr.boxCrashSound.setRate(MathUtils.random(.9f, 1.1f));
            ResourcesMgr.biteSound.play();

            //Splat
            PointParticleEmitter particleEmitter = new PointParticleEmitter(LIVES_BAR_POSITION_X + ((mLivesLeft - 1) * ITEM_LIFE_WIDTH), LIVES_BAR_POSITION_Y);
            final SpriteParticleSystem particleSystem = new SpriteParticleSystem(particleEmitter, 1000, 1000, 1, ResourcesMgr.splatTexture, ResourcesMgr.getInstance().vertexBufferObjectManager);
            particleSystem.addParticleInitializer(new AlphaParticleInitializer<Sprite>(1f));
            particleSystem.addParticleInitializer(new ColorParticleInitializer<Sprite>(APPLE_SPLAT_COLOR));
            particleSystem.addParticleInitializer(new RotationParticleInitializer<Sprite>((float) (Math.random() * 2 * Math.PI)));
            particleSystem.addParticleModifier(new AlphaParticleModifier<Sprite>(0, 1f, 1f, 0f));
            hud.attachChild(particleSystem);

            mLivesLeft--;
            livesBarMarkedToUpdate = true;
        }
    }


    private void showLoseScreen() {
        hud.unregisterTouchArea(powerSlider);
        Rectangle blackScreen = new Rectangle(camera.getWidth()/2,camera.getHeight()/2,camera.getWidth(), camera.getHeight(), vertexBufferObjectManager);
        blackScreen.setColor(0,0,0,.8f);
        hud.attachChild(blackScreen);

        ShadowText youLoseText = new ShadowText(Constants.CAMERA_WIDTH/2 , Constants.CAMERA_HEIGHT/2 , ResourcesMgr.grinchedLargeFont, "HA HA! You lose", new TextOptions(HorizontalAlign.CENTER), vertexBufferObjectManager);
        youLoseText.setColor(new Color(255f/255,184f/255,33f/255));
        hud.attachChild(youLoseText);

        nextLevelText = new Text(Constants.CAMERA_WIDTH/2  , Constants.CAMERA_HEIGHT/2 - 100, ResourcesMgr.grinchedMediumFont, "Press here to try again", new TextOptions(HorizontalAlign.CENTER), vertexBufferObjectManager)  {
            @Override
            public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionDown()) {
                    ResourcesMgr.losingSound.stop();
                    resetLevel(currentLevel);
                }
                return true;
            }

        };

        if (ResourcesMgr.marketMusic.isPlaying()) {
            ResourcesMgr.marketMusic.pause();
        }
        ResourcesMgr.losingSound.play();

        nextLevelText.setColor(Color.WHITE);
        hud.attachChild(nextLevelText);

        engine.registerUpdateHandler(new TimerHandler(2f, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                hud.registerTouchArea(nextLevelText);
            }
        }));

    }

    private void showWinScreen() {
        hud.unregisterTouchArea(powerSlider);
        Rectangle blackScreen = new Rectangle(camera.getWidth()/2,camera.getHeight()/2,camera.getWidth(), camera.getHeight(), vertexBufferObjectManager) ;
        blackScreen.setColor(0,0,0,.8f);
        hud.attachChild(blackScreen);

        String text;
        if (currentLevel == LevelDef.AvailableLevels.length - 2) {
            text = "Get ready for the FINAL LEVEL!";
        }
        else if (currentLevel == LevelDef.AvailableLevels.length - 1) {
            text = "Congrats! You defeated the rotten fruit!";
        }
        else {
            text = "Level " + (currentLevel + 1) + " completed!";
        }

        ShadowText youWinText = new ShadowText(Constants.CAMERA_WIDTH/2 , Constants.CAMERA_HEIGHT/2 , ResourcesMgr.grinchedLargeFont, text, new TextOptions(HorizontalAlign.CENTER), vertexBufferObjectManager);
        youWinText.setColor(new Color(255f/255,184f/255,33f/255));
        hud.attachChild(youWinText);

        if (currentLevel == LevelDef.AvailableLevels.length - 1) {
            text = "Press here to return to menu";
        }
        else {
            text = "Press here to continue";
        }

        nextLevelText = new Text(Constants.CAMERA_WIDTH/2  , Constants.CAMERA_HEIGHT/2 - 100, ResourcesMgr.grinchedMediumFont, text, new TextOptions(HorizontalAlign.CENTER), vertexBufferObjectManager){
            @Override
            public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionDown()) {
                    ResourcesMgr.winningSound.stop();
                    if (currentLevel == LevelDef.AvailableLevels.length - 1) {
                        // End of the game
                        SceneMgr.getInstance().gameScene.disposeScene();
                        SceneMgr.getInstance().gameScene = null;
                        SceneMgr.getInstance().createMenuScene();
                    } else {
                        loadLevel(currentLevel + 1);
                    }
                }
                return true;
            }

        };
        if (ResourcesMgr.marketMusic.isPlaying()) {
            ResourcesMgr.marketMusic.pause();
        }
        ResourcesMgr.winningSound.play();

        nextLevelText.setColor(Color.WHITE);
        hud.attachChild(nextLevelText);

        engine.registerUpdateHandler(new TimerHandler(2f, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                hud.registerTouchArea(nextLevelText);
            }
        }));

    }

    public ArrayList<String> createEnemiesList(LevelDef level) {
        ArrayList<String> shuffledList = new ArrayList<String>();
        for (int i = 0; i<level.amountApple; i++)
            shuffledList.add("0");

        for (int i = 0; i<level.amountPear; i++)
            shuffledList.add("1");

        for (int i = 0; i<level.amountBanana; i++)
            shuffledList.add("2");

        for (int i = 0; i<level.amountGrape; i++)
            shuffledList.add("4");
        Collections.shuffle(shuffledList);

        //Insert pauses between hordes
        int hordeSize = shuffledList.size() / level.hordes;
        for (int i = 1; i< level.hordes; i++) {
            shuffledList.add(i*hordeSize, "P");
        }

        // Insert watermelon at the end of a horde
        int watermelons = (int)level.amountWatermelon;
        if (watermelons > 0) {
            shuffledList.add("3");
            watermelons--;
        }
        int i = shuffledList.size() -1;
        while (i>=0 && watermelons>0){
            if (shuffledList.get(i) == "P") {
                shuffledList.add(i, "3");
                watermelons --;
            }
            i--;
        }

        return shuffledList;

    }

    public void pause () {
        if (ResourcesMgr.marketMusic.isPlaying()) {
            ResourcesMgr.marketMusic.pause();
        }
        lastCameraPosition = new Vector2 (camera.getCenterX(), camera.getCenterY());
        lastCameraZoomFactor = camera.getZoomFactor();
        this.gameState = GameState.PAUSED;
    }

    public void resume () {
        if (!ResourcesMgr.marketMusic.isPlaying() && ResourcesMgr.getInstance().gameOptions.musicEnabled) {
            ResourcesMgr.marketMusic.play();
        }
        initializeCamera();
        camera.setCenterDirect(lastCameraPosition.x, lastCameraPosition.y);
        camera.setZoomFactorDirect(lastCameraZoomFactor);
        this.gameState = GameState.PLAYING;

    }


    public void notifyEnemyKilled(Enemy enemy) {
        if (comboTimeCounter >0) {
            /* Show combo */
            ShadowText text = new ShadowText(enemy.mEntity.getX(), enemy.mEntity.getY() + 200, ResourcesMgr.grinchedMediumFont, comboKillsCounter + 1 + "x COMBO!", new TextOptions(HorizontalAlign.CENTER), vertexBufferObjectManager);
            text.setColor(SCORE_TEXT_COLOR);
            FadingText combo = new FadingText (text, this);
            this.getChildByIndex(Constants.LAYER_EFFECTS).attachChild(combo.mText);
            this.registerUpdateHandler(combo);

            itemsBar.notifyKill(); // Combo counts as an extra kill
        }

        comboKillsCounter ++;
        comboTimeCounter = COMBO_TIME_WINDOW;
        enemiesKilled ++ ;

        itemsBar.notifyKill();


    }

    public void resetEnemySpawnTime() {
        enemySpawnTime = levelDef.initialSpawnTime;
    }

    public void  showAllChildrenCount (GameScene s){

        //String msg = "All entities count: Bg=" + getAllChildrenCount((Entity)s.getChildByIndex(Constants.LAYER_BACKGROUND)) + ", ";
        //string msg = "Items=" + getAllChildrenCount((Entity)s.getChildByIndex(Constants.LAYER_ITEMS)) + ", ";
        //String msg = "Chars=" + getAllChildrenCount((Entity)s.getChildByIndex(Constants.LAYER_CHARACTERS)) + ", ";
//        msg += "Effects=" + getAllChildrenCount((Entity)s.getChildByIndex(Constants.LAYER_EFFECTS)) + ", ";
//        msg += "Debug=" + getAllChildrenCount((Entity)s.getChildByIndex(Constants.LAYER_DEBUG));

        String msg = "All entities count: " + getAllChildrenCount(this);
        Log.d(Constants.TAG, msg);

    }

    public int getAllChildrenCount (Entity e){
        int count = 1;
        for (int i=0; i<e.getChildCount(); i++) {
            count += getAllChildrenCount((Entity)e.getChildByIndex(i));
        }
        return count;
    }

}
