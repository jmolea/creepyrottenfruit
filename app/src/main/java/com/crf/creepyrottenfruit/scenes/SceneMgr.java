package com.crf.creepyrottenfruit.scenes;

import com.crf.creepyrottenfruit.core.BaseScene;
import com.crf.creepyrottenfruit.core.ResourcesMgr;

import org.andengine.engine.Engine;
import org.andengine.ui.IGameInterface;

/*
 * Created by Jose Maria Olea on 30/12/2017.
 */

public class SceneMgr {

    private BaseScene splashScene;
    private BaseScene menuScene;
    public BaseScene gameScene;
    private BaseScene loadingScene;

    private static final SceneMgr INSTANCE = new SceneMgr();

    private SceneType currenteSceneType;
    private BaseScene currentScene;
    private Engine engine = ResourcesMgr.getInstance().engine;

    public enum SceneType  {
        SCENE_SPLASH,
        SCENE_MENU,
        SCENE_GAME,
        SCENE_LOADING
    }

    public void setScene (BaseScene scene) {
        engine.setScene(scene);
        currentScene = scene;
        currenteSceneType = scene.getSceneType();
    }

    public void setScene (SceneType sceneType) {
        switch (sceneType) {
            case SCENE_MENU:
                setScene(menuScene);
                break;
            case SCENE_SPLASH:
                setScene(splashScene);
                break;
            case SCENE_GAME:
                setScene(gameScene);
                break;
            case SCENE_LOADING:
                setScene(loadingScene);
                break;
        }
    }

    public static SceneMgr getInstance() {
        return INSTANCE;
    }

    public SceneType getCurrenteSceneType() {
        return currenteSceneType;
    }

    public BaseScene getCurrentScene () {
        return currentScene;
    }

    public void createSplashScene (IGameInterface.OnCreateSceneCallback callback) {
        ResourcesMgr.getInstance().loadSplashScreen();
        splashScene = new SplashScene();
        currentScene = splashScene;
        callback.onCreateSceneFinished(splashScene);
    }

    public void disposeSplashScene () {
        ResourcesMgr.getInstance().unloadSplashScreen();
        splashScene.disposeScene();
        splashScene = null;
    }


    public void createMenuScene () {
        ResourcesMgr.getInstance().loadMenuScreen();
        menuScene = new MenuScene();
        setScene(menuScene);
    }

    public void disposeMenuScene () {
        ResourcesMgr.getInstance().unloadMenuScreen();
        menuScene.disposeScene();
        menuScene = null;
    }

    public void createGameScene () {
        if (gameScene == null) {
            ResourcesMgr.getInstance().loadGameResources();
            gameScene = new GameScene();
        } else {
            ((GameScene)gameScene).resetGame();
        }
        setScene(gameScene);
    }

    public void continueGame () {
        if (gameScene != null ){
            this.gameScene.resume();
            setScene(this.gameScene);
        }
    }

    public void disposeGameScene () {
        ResourcesMgr.getInstance().unloadGameResources();
        gameScene.disposeScene();
        gameScene = null;
    }
}
