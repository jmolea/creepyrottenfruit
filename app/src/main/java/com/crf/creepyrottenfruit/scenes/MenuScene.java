package com.crf.creepyrottenfruit.scenes;

import android.view.View;
import android.widget.Button;

import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.BaseScene;
import com.crf.creepyrottenfruit.core.ResourcesMgr;

import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.util.adt.color.Color;

/*
 * Created by Jose Maria Olea on 30/12/2017.
 */

public class MenuScene extends BaseScene {

    // ====================================================
    // CONSTANTS
    // ====================================================


    private final float MENU_POSITION_X = Constants.CAMERA_WIDTH/2 + 350;
    private final float MENU_POSITION_Y = Constants.CAMERA_HEIGHT/2 + 150;
    private final float VERTICAL_SEPARATION_OPTIONS = 150;


    // ====================================================
    // VARIABLES
    // ====================================================

    private Sprite splash, title, optionNewGame, optionContinue;
    private TiledSprite musicToggleButton;


    // ====================================================
    // CONSTRUCTOR
    // ====================================================

    public MenuScene() {


        super();
        createScene();
        initializeCamera ();
    }

    // ====================================================
    // METHODS
    // ====================================================

    @Override
    public void createScene() {

        splash = new Sprite(Constants.CAMERA_WIDTH/2, Constants.CAMERA_HEIGHT/2, ResourcesMgr.splashBgTexture, vertexBufferObjectManager);
        title = new Sprite(Constants.CAMERA_WIDTH/2 - 220, Constants.CAMERA_HEIGHT/2, ResourcesMgr.splashTitleTexture, vertexBufferObjectManager);
        attachChild(splash);
        attachChild(title);


        /** New game */

        float verticalOffset = MENU_POSITION_Y;
        optionNewGame = new Sprite(MENU_POSITION_X,verticalOffset, ResourcesMgr.optionNewTexture, vertexBufferObjectManager) {
            @Override
            public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionDown()) {
                    ResourcesMgr.clickSound.play();
                    SceneMgr.getInstance().disposeMenuScene();
                    SceneMgr.getInstance().createGameScene();
                }
                return true;
            }
        };
        attachChild(optionNewGame);
        registerTouchArea(optionNewGame);
        verticalOffset -= VERTICAL_SEPARATION_OPTIONS;


        /** Continue game */

        if (SceneMgr.getInstance().gameScene != null) {
            optionContinue = new Sprite(MENU_POSITION_X , verticalOffset, ResourcesMgr.optionContinueTexture, vertexBufferObjectManager) {
                @Override
                public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.isActionDown()) {
                        ResourcesMgr.clickSound.play();
                        SceneMgr.getInstance().disposeMenuScene();
                        SceneMgr.getInstance().continueGame();
                    }
                    return true;
                }
            };
            attachChild(optionContinue);
            registerTouchArea(optionContinue);

        }



        /** Music Toggle */

        musicToggleButton = new TiledSprite(Constants.CAMERA_WIDTH - 80, 80, ResourcesMgr.musicIconTexture, vertexBufferObjectManager) {
            @Override
            public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionDown()) {
                    ResourcesMgr.clickSound.play();
                    ResourcesMgr.getInstance().gameOptions.musicEnabled = ! ResourcesMgr.getInstance().gameOptions.musicEnabled;
                    musicToggleButton.setCurrentTileIndex(ResourcesMgr.getInstance().gameOptions.musicEnabled?0:1);
                }
                return true;
            }
        };
        musicToggleButton.setCurrentTileIndex(ResourcesMgr.getInstance().gameOptions.musicEnabled?0:1);
        attachChild(musicToggleButton);
        registerTouchArea(musicToggleButton);

        /** Version */
        Text versionText =  new Text(150,50,ResourcesMgr.defaultSmallFont, Constants.VERSION, ResourcesMgr.getInstance().vertexBufferObjectManager);
        versionText.setColor( new Color(202f/255,196f/255,182f/255) );
        attachChild(versionText);

    }

    public void initializeCamera() {
        camera.reset();
    }

    @Override
    public void onBackKeyPressed() {

    }

    @Override
    public SceneMgr.SceneType getSceneType() {
        return SceneMgr.SceneType.SCENE_SPLASH;
    }

    @Override
    public void disposeScene() {
        splash.detachSelf();
        splash.dispose();
        title.detachSelf();
        title.dispose();
        optionNewGame.detachSelf();
        optionNewGame.dispose();
        musicToggleButton.detachSelf();
        musicToggleButton.dispose();
        this.detachSelf();
        this.dispose();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }
}
