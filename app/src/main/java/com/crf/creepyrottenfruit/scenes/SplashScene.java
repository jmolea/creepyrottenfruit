package com.crf.creepyrottenfruit.scenes;

import com.crf.creepyrottenfruit.Constants;
import com.crf.creepyrottenfruit.core.BaseScene;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.util.GLState;

/*
 * Created by Jose Maria Olea on 30/12/2017.
 */

public class SplashScene extends BaseScene {

    private Sprite splash, title;

    public SplashScene () {
        super();
        createScene();
    }

    @Override
    public void createScene() {
        splash = new Sprite(0,0, resourcesMgr.splashBgTexture, vertexBufferObjectManager);
        title = new Sprite(0,0, resourcesMgr.splashTitleTexture, vertexBufferObjectManager);

        splash.setPosition(Constants.CAMERA_WIDTH/2, Constants.CAMERA_HEIGHT/2);
        attachChild(splash);
        title.setPosition(Constants.CAMERA_WIDTH/2 - 220, Constants.CAMERA_HEIGHT/2);
        attachChild(title);
    }

    @Override
    public void onBackKeyPressed() {

    }

    @Override
    public SceneMgr.SceneType getSceneType() {
        return SceneMgr.SceneType.SCENE_SPLASH;
    }

    @Override
    public void disposeScene() {
        splash.detachSelf();
        splash.dispose();
        title.detachSelf();
        title.dispose();
        this.detachSelf();
        this.dispose();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }
}
